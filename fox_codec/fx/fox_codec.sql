-- MySQL dump 10.13  Distrib 5.5.59, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: iptv
-- ------------------------------------------------------
-- Server version	5.5.59-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `stream_id` int(11) NOT NULL,
  `str_type` int(11) DEFAULT '1',
  `user_ip` varchar(255) NOT NULL,
  `os` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `pid` int(11) NOT NULL,
  `date_start` varchar(255) NOT NULL,
  `date_end` varchar(500) DEFAULT NULL,
  `container` varchar(255) DEFAULT NULL,
  `last_ts` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `date_end` (`date_end`(255))
) ENGINE=InnoDB AUTO_INCREMENT=16569 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity`
--

LOCK TABLES `activity` WRITE;
/*!40000 ALTER TABLE `activity` DISABLE KEYS */;
INSERT INTO `activity` VALUES (12172,6,3139,1,'198.27.70.220','','CA',25448,'2018-02-23 19:13:51','0',NULL,NULL),(15933,6,3170,1,'198.27.70.220','','CA',23951,'2018-02-23 20:07:55','0',NULL,NULL);
/*!40000 ALTER TABLE `activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `administrator`
--

DROP TABLE IF EXISTS `administrator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `create_date` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrator`
--

LOCK TABLES `administrator` WRITE;
/*!40000 ALTER TABLE `administrator` DISABLE KEYS */;
INSERT INTO `administrator` VALUES (1,'fox','fox5656','23-02-2018 05:37:40');
/*!40000 ALTER TABLE `administrator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_videos`
--

DROP TABLE IF EXISTS `alert_videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_stop_video` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `stop_status` int(11) DEFAULT '0',
  `member_finish_video` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `buy_status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_videos`
--

LOCK TABLES `alert_videos` WRITE;
/*!40000 ALTER TABLE `alert_videos` DISABLE KEYS */;
INSERT INTO `alert_videos` VALUES (1,'/home/fox_codec/alert_videos/leadepro2.mp4',1,'/home/fox_codec/alert_videos/leadepro2.mp4',1);
/*!40000 ALTER TABLE `alert_videos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banned_country`
--

DROP TABLE IF EXISTS `banned_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banned_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ulke` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ulke_adi` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banned_country`
--

LOCK TABLES `banned_country` WRITE;
/*!40000 ALTER TABLE `banned_country` DISABLE KEYS */;
/*!40000 ALTER TABLE `banned_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banned_ip`
--

DROP TABLE IF EXISTS `banned_ip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banned_ip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banned_ip`
--

LOCK TABLES `banned_ip` WRITE;
/*!40000 ALTER TABLE `banned_ip` DISABLE KEYS */;
/*!40000 ALTER TABLE `banned_ip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel_category`
--

DROP TABLE IF EXISTS `channel_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category_abbreviation` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL COMMENT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel_category`
--

LOCK TABLES `channel_category` WRITE;
/*!40000 ALTER TABLE `channel_category` DISABLE KEYS */;
INSERT INTO `channel_category` VALUES (5,'CANAL','Br',NULL);
/*!40000 ALTER TABLE `channel_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channels`
--

DROP TABLE IF EXISTS `channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `channel_logo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `channel_path_1` varchar(500) CHARACTER SET utf8mb4 NOT NULL,
  `channel_path_2` varchar(500) CHARACTER SET utf8 NOT NULL,
  `channel_path_3` varchar(500) CHARACTER SET utf8 NOT NULL,
  `server_id` int(11) NOT NULL DEFAULT '1',
  `category_id` int(11) NOT NULL,
  `transcode_profiles` int(11) NOT NULL DEFAULT '0',
  `transcoding` int(11) NOT NULL DEFAULT '0',
  `sort` int(255) NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `pid` int(11) NOT NULL DEFAULT '0',
  `stream_status` int(11) NOT NULL DEFAULT '0',
  `start_time` varchar(255) CHARACTER SET utf8 NOT NULL,
  `stop_time` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pid_control` int(11) NOT NULL DEFAULT '0',
  `pid_control_ok` int(11) NOT NULL DEFAULT '0',
  `control_count` int(11) NOT NULL DEFAULT '0',
  `output` varchar(255) CHARACTER SET utf8 NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `rtmp_username` varchar(255) CHARACTER SET utf8 NOT NULL,
  `rtmp_password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `hls_username` varchar(255) CHARACTER SET utf8 NOT NULL,
  `hls_password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `direct_stream` int(11) NOT NULL DEFAULT '0',
  `sleep_mod` int(255) NOT NULL DEFAULT '0',
  `channel_type` int(11) NOT NULL DEFAULT '1',
  `channel_id` varchar(255) CHARACTER SET utf8 NOT NULL,
  `epg_id` int(11) NOT NULL DEFAULT '0',
  `epg_lang` varchar(255) CHARACTER SET utf8 NOT NULL,
  `working_channel` int(11) NOT NULL DEFAULT '1',
  `restream` int(11) NOT NULL DEFAULT '0',
  `restream_status` int(11) NOT NULL DEFAULT '0',
  `vod_channel_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `vod_channel_url` varchar(255) CHARACTER SET utf8 NOT NULL,
  `test_pid` int(11) NOT NULL,
  `test_status` int(11) NOT NULL DEFAULT '0',
  `playlist_status` int(11) NOT NULL DEFAULT '0',
  `channel_directory` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ffmpeg_command` varchar(500) CHARACTER SET utf8 DEFAULT '0',
  `sira` int(255) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `channel_id` (`id`) USING BTREE,
  KEY `server_id` (`server_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3190 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channels`
--

LOCK TABLES `channels` WRITE;
/*!40000 ALTER TABLE `channels` DISABLE KEYS */;
INSERT INTO `channels` VALUES (3124,'RETRO CARTOON','/channel_logo/resimyok.gif','http://stmv2.srvstm.com/cartoonr/cartoonr/playlist.m3u8','','',1,5,0,0,1,5,16675,1,'23-02-2018 14:54:39','',0,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',1),(3125,'ANIME TV','/channel_logo/resimyok.gif','http://video7.stream.mx:1935/raxatv/raxatv/chunklist_w884577146.m3u8','','',1,5,0,0,2,1,15583,1,'23-02-2018 14:40:01','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',2),(3126,'TV APARECIDA','/channel_logo/resimyok.gif','http://caikrondatacenter.com.br:1935/tvaparecida/tvaparecida.stream/playlist.m3u8','','',1,5,0,0,3,0,0,1,'23-02-2018 14:38:55','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',3),(3127,'TV APARECIDA ','https://lut.im/4dpYzeY5ga/LxqrxrjkXcIeOSn1.png','http://caikrondatacenter.com.br:1935/tvaparecida/tvaparecida.stream/playlist.m3u8','','',1,5,0,0,4,0,0,1,'23-02-2018 14:38:56','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',4),(3128,'REDE VIDA','https://goo.gl/H4GFo1','http://rtmp.cdn.upx.net.br:1935/74cc/myStream.sdp/live.m3u8','','',1,5,0,0,5,0,0,1,'23-02-2018 14:38:57','',0,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',5),(3129,'CANCAO NOVA ','https://goo.gl/63ISSk','http://cancaonova-push-live.hls.adaptive.level3.net:80/hls-live/cancaonova-channel01/_definst_/live/stream1.m3u8','','',1,5,0,0,6,0,0,1,'23-02-2018 14:39:06','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',6),(3130,'TV CANÇÃO NOVA HD ','http://s20.postimg.org/athr2e65p/TV_Can_o_Nova.png','http://cancaonova-push-live.hls.adaptive.level3.net/hls-live/cancaonova-channel01/_definst_/live/stream1.m3u8','','',1,5,0,0,7,0,0,1,'23-02-2018 14:39:07','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',7),(3131,'REDE SECULO 21 ','http://s14.postimg.org/k9exx981d/Rede_S_culo_21.png','http://tvseculo21-lh.akamaihd.net/i/tvseculo_1@16110/master.m3u8','','',1,5,0,0,8,0,0,1,'23-02-2018 14:39:07','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',8),(3132,'REDE GOSPEL ','https://lut.im/M6XzAMj8cC/hpRcOL0Jv7rvMGmH.png','http://media.igospel.com.br:1935/live/redegospel/waldo_paes.m3u8','','',1,5,0,0,9,0,0,1,'23-02-2018 14:39:11','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',9),(3133,'TV TERCEIRO ANJO ','https://goo.gl/G7hM3f','http://streamer1.streamhost.org:1935/salive/GMI3anjoh/playlist.m3u8','','',1,5,0,0,10,0,0,1,'23-02-2018 14:39:12','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',10),(3134,'TV UNIVERSAL ','http://s20.postimg.org/hsnwlbh65/TV_Universal.jpg','http://api.new.livestream.com/accounts/4486152/events/2780306/live.m3u8','','',1,5,0,0,11,0,0,1,'23-02-2018 14:39:14','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',11),(3135,'REDE SUPER ','http://s18.postimg.org/j1yr3lukp/REDE_SUPER.png','http://api.new.livestream.com/accounts/10205943/events/3429501/live.m3u8','','',1,5,0,0,12,0,0,1,'23-02-2018 14:39:21','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',12),(3136,'TV LEÃO DE JUDÁ ','https://goo.gl/egHSeo','http://streaming42.sitehosting.com.br:1935/juda/juda/live.m3u8','','',1,5,0,0,13,0,0,1,'23-02-2018 14:39:24','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',13),(3137,'CATHOLIC TV ','https://goo.gl/c6FKZb','http://catholictvhd-lh.akamaihd.net:80/i/ctvhd_1@88148/index_1_av-p.m3u8','','',1,5,0,0,14,0,0,1,'23-02-2018 14:39:24','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',14),(3138,'PORTAL GOSPEL TV','/channel_logo/resimyok.gif','http://s1.wsepanel.com:1935/lukfilm/lukfilm/live.m3u8','','',1,5,0,0,15,0,0,1,'23-02-2018 14:39:25','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',15),(3139,'ANIMAL PLANET HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/370.ts','','',1,5,0,0,16,345,18576,1,'23-02-2018 15:24:29','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',16),(3140,'ANIMAL PLANET SD - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/470.ts','','',1,5,0,0,17,330,18588,1,'23-02-2018 15:24:34','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',17),(3141,'DISCOVERY CHANNEL FHD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/459.ts','','',1,5,0,0,18,332,20263,1,'23-02-2018 19:59:53','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',18),(3142,'DISCOVERY CHANNEL HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/351.ts','','',1,5,0,0,19,332,18607,1,'23-02-2018 15:24:39','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',19),(3143,'DISCOVERY CHANNEL SD - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/460.ts','','',1,5,0,0,20,327,18620,1,'23-02-2018 15:24:44','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',20),(3144,'DISCOVERY CIVILIZATION HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/350.ts','','',1,5,0,0,21,330,18633,1,'23-02-2018 15:24:48','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',21),(3145,'DISCOVERY HOME & HEALTH HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/349.ts','','',1,5,0,0,22,322,18646,1,'23-02-2018 15:24:53','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',22),(3146,'DISCOVERY HOME & HEALTH SD - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/458.ts','','',1,5,0,0,23,327,20327,1,'23-02-2018 19:59:58','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',23),(3147,'DISCOVERY SCIENCE SD - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/456.ts','','',1,5,0,0,24,311,27098,1,'23-02-2018 20:11:58','',0,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',24),(3148,'DISCOVERY THEATER HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/347.ts','','',1,5,0,0,25,324,18734,1,'23-02-2018 15:25:06','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',25),(3149,'DISCOVERY TURBO FHD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/398.ts','','',1,5,0,0,26,335,20544,1,'23-02-2018 20:00:10','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',26),(3150,'DISCOVERY TURBO HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/346.ts','','',1,5,0,0,27,330,27076,1,'23-02-2018 20:11:57','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',27),(3151,'DISCOVERY TURBO SD - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/455.ts','','',1,5,0,0,28,335,16964,1,'23-02-2018 14:59:19','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',28),(3152,'DISCOVERY WORLD  HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/345.ts','','',1,5,0,0,29,321,18782,1,'23-02-2018 15:25:41','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',29),(3153,'DW DEUTSCH - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/452.ts','','',1,5,0,0,30,321,18798,1,'23-02-2018 15:25:49','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',30),(3154,'H2 HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/317.ts','','',1,5,0,0,31,323,18811,1,'23-02-2018 15:26:00','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',31),(3155,'ID - INVESTIGAÇÃO DISCOVERY - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/426.ts','','',1,5,0,0,32,323,18885,1,'23-02-2018 15:26:12','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',32),(3156,'ID - INVESTIGATION DISCOVERY HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/310.ts','','',1,5,0,0,33,321,18897,1,'23-02-2018 15:26:20','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',33),(3157,'HISTORY CHANNEL FHD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/427.ts','','',1,5,0,0,34,325,3041,1,'23-02-2018 16:05:09','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',34),(3158,'HISTORY CHANNEL HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/311.ts','','',1,5,0,0,35,327,18919,1,'23-02-2018 15:26:30','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',35),(3159,'NAT GEO HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/302.ts','','',1,5,0,0,36,322,23129,1,'23-02-2018 20:04:39','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',36),(3160,'NATGEO WILD HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/300.ts','','',1,5,0,0,37,324,18944,1,'23-02-2018 15:26:46','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',37),(3161,'BAND FULL HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/368.ts','','',1,5,0,0,38,317,27629,1,'23-02-2018 20:12:16','',0,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',38),(3162,'BAND HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/367.ts','','',1,5,0,0,39,338,27648,1,'23-02-2018 20:12:18','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',39),(3163,'BAND NEWS HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/366.ts','','',1,5,0,0,40,315,27731,1,'23-02-2018 20:12:25','',0,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',40),(3164,'BAND SP SD - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/467.ts','','',1,5,0,0,41,312,15639,1,'23-02-2018 19:52:39','',0,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',41),(3165,'CANAL BRASIL HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/360.ts','','',1,5,0,0,42,321,19065,1,'23-02-2018 15:27:23','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',42),(3166,'CNN INTERNACIONAL - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/462.ts','','',1,5,0,0,43,315,14655,1,'23-02-2018 19:50:46','',0,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',43),(3167,'EPTV CAMPINAS HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/339.ts','','',1,5,0,0,44,327,27793,1,'23-02-2018 20:12:30','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',44),(3168,'EPTV CAMPINAS SD - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/451.ts','','',1,5,0,0,45,326,19104,1,'23-02-2018 15:27:45','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',45),(3169,'EPTV RIBEIRAO PRETO - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/450.ts','','',1,5,0,0,46,329,26890,1,'23-02-2018 20:11:50','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',46),(3170,'EPTV SAO CARLOS - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/449.ts','','',1,5,0,0,47,336,19144,1,'23-02-2018 15:27:55','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',47),(3171,'GLOBO AMAZONICA MANAUS - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/441.ts','','',1,5,0,0,48,315,14832,1,'23-02-2018 19:51:06','',0,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',48),(3172,'GLOBO BRASILIA HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/328.ts','','',1,5,0,0,49,333,19228,1,'23-02-2018 15:28:10','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',49),(3173,'GLOBO INTER TV CABUGI - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/440.ts','','',1,5,0,0,50,313,20952,1,'23-02-2018 20:00:39','',0,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',50),(3174,'GLOBO INTERNACIONAL FHD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/327.ts','','',1,5,0,0,51,333,19257,1,'23-02-2018 15:28:23','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',51),(3175,'Globo Minas FHD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/379.ts','','',1,5,0,0,52,321,28888,1,'23-02-2018 20:13:56','',0,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',52),(3176,'GLOBO MINAS HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/326.ts','','',1,5,0,0,53,330,28294,1,'23-02-2018 20:12:49','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',53),(3177,'GLOBO NEWS HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/325.ts','','',1,5,0,0,54,325,22306,1,'23-02-2018 18:01:53','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',54),(3178,'GLOBO NORDESTE - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/439.ts','','',1,5,0,0,55,313,21248,1,'23-02-2018 20:00:52','',0,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',55),(3179,'GLOBO NORDESTE HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/324.ts','','',1,5,0,0,56,332,28358,1,'23-02-2018 20:12:51','',0,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',56),(3180,'GLOBO PARANA  HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/323.ts','','',1,5,0,0,57,326,23879,1,'23-02-2018 20:07:14','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',57),(3181,'Globo RJ FHD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/378.ts','','',1,5,0,0,58,313,28399,1,'23-02-2018 20:12:52','',0,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',58),(3182,'GLOBO RJ HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/322.ts','','',1,5,0,0,59,337,27678,1,'23-02-2018 20:12:20','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',59),(3183,'GLOBO RJ HD (globosat) - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/438.ts','','',1,5,0,0,60,314,28460,1,'23-02-2018 20:13:01','',0,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',60),(3184,'GLOBO SAO JOSE DOS CAMPOS - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/437.ts','','',1,5,0,0,61,324,26615,1,'23-02-2018 20:10:29','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',61),(3185,'GLOBO SP - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/436.ts','','',1,5,0,0,62,328,23564,1,'23-02-2018 20:06:10','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',62),(3186,'GLOBO SP HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/321.ts','','',1,5,0,0,63,326,19462,1,'23-02-2018 15:29:43','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',63),(3187,'GLOBO SP HD (globoplay) - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/435.ts','','',1,5,0,0,64,319,28667,1,'23-02-2018 20:13:11','',0,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',64),(3188,'GLOBO TV BAHIA - .','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/434.ts','','',1,5,0,0,65,312,15281,1,'23-02-2018 19:51:40','',0,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',65),(3189,'GLOBOSAT HD','/channel_logo/resimyok.gif','http://painelcinetv.ml:25461/live/fox/fox/320.ts','','',1,5,0,0,66,311,19562,1,'23-02-2018 15:30:06','',1,1,0,'hls','','','','','',0,1,1,'',0,'',1,0,0,'','',0,0,0,'','false',66);
/*!40000 ALTER TABLE `channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channels_map`
--

DROP TABLE IF EXISTS `channels_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channels_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ch_id` int(11) NOT NULL,
  `map_wr` varchar(500) NOT NULL,
  `codec_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channels_map`
--

LOCK TABLES `channels_map` WRITE;
/*!40000 ALTER TABLE `channels_map` DISABLE KEYS */;
INSERT INTO `channels_map` VALUES (1,624,'-map 0:0','video'),(2,624,'-map 0:1','audio'),(3,3002,'-map 0:0','video');
/*!40000 ALTER TABLE `channels_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countriess`
--

DROP TABLE IF EXISTS `countriess`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countriess` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `AC` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countriess`
--

LOCK TABLES `countriess` WRITE;
/*!40000 ALTER TABLE `countriess` DISABLE KEYS */;
INSERT INTO `countriess` VALUES (1,'AC','Ascension Island',0),(2,'AD','Andorra',0),(3,'AE','United Arab Emirates',0),(4,'AF','Afghanistan',0),(5,'AG','Antigua and Barbuda',0),(6,'AI','Anguilla',0),(7,'AL','Albania',0),(8,'AM','Armenia',0),(9,'AN','Netherlands Antilles',0),(10,'AO','Angola',0),(11,'AQ','Antarctica',0),(12,'AR','Argentina',0),(13,'AS','American Samoa',0),(14,'AT','Austria',0),(15,'AU','Australia',0),(16,'AW','Aruba',0),(17,'AX','Ã…land Islands',0),(18,'AZ','Azerbaijan',0),(19,'BA','Bosnia and Herzegovina',0),(20,'BB','Barbados',0),(21,'BD','Bangladesh',0),(22,'BE','Belgium',0),(23,'BF','Burkina Faso',0),(24,'BG','Bulgaria',0),(25,'BH','Bahrain',0),(26,'BI','Burundi',0),(27,'BJ','Benin',0),(28,'BL','Saint BarthÃ©lemy',0),(29,'BM','Bermuda',0),(30,'BN','Brunei Darussalam',0),(31,'BO','Bolivia',0),(32,'BR','Brazil',0),(33,'BS','Bahamas',0),(34,'BT','Bhutan',0),(35,'BV','Bouvet Island',0),(36,'BW','Botswana',0),(37,'BY','Belarus',0),(38,'BZ','Belize',0),(39,'CA','Canada',18358),(40,'CC','Cocos (Keeling) Islands',0),(41,'CD','Congo, Democratic Republic of the',0),(42,'CF','Central African Republic',0),(43,'CG','Congo, Republic of the',0),(44,'CH','Switzerland',0),(45,'CI','CÃ´te d\'Ivoire',0),(46,'CK','Cook Islands',0),(47,'CL','Chile',0),(48,'CM','Cameroon',0),(49,'CN','China',0),(50,'CO','Colombia',0),(51,'CR','Costa Rica',0),(52,'CU','Cuba',0),(53,'CV','Cape Verde',0),(54,'CX','Christmas Island',0),(55,'CY','Cyprus',0),(56,'CZ','Czech Republic',0),(57,'DE','Germany',0),(58,'DJ','Djibouti',0),(59,'DK','Denmark',0),(60,'DM','Dominica',0),(61,'DO','Dominican Republic',0),(62,'DZ','Algeria',0),(63,'EC','Ecuador',0),(64,'EE','Estonia',0),(65,'EG','Egypt',0),(66,'EH','Western Sahara',0),(67,'ER','Eritrea',0),(68,'ES','Spain',0),(69,'ET','Ethiopia',0),(70,'FI','Finland',0),(71,'FJ','Fiji',0),(72,'FK','Falkland Islands (Malvinas)',0),(73,'FM','Federated States of Micronesia',0),(74,'FO','Faroe Islands',0),(75,'FR','France',0),(76,'GA','Gabon',0),(77,'GB','United Kingdom',0),(78,'GD','Grenada',0),(79,'GE','Georgia',0),(80,'GF','French Guiana',0),(81,'GG','Guernsey',0),(82,'GH','Ghana',0),(83,'GI','Gibraltar',0),(84,'GL','Greenland',0),(85,'GM','Gambia',0),(86,'GN','Guinea',0),(87,'GP','Guadeloupe',0),(88,'GQ','Equatorial Guinea',0),(89,'GR','Greece',0),(90,'GS','South Georgia and the South Sandwich Islands',0),(91,'GT','Guatemala',0),(92,'GU','Guam',0),(93,'GW','Guinea-Bissau',0),(94,'GY','Guyana',0),(95,'HK','Hong Kong',0),(96,'HM','Heard Island and McDonald Islands',0),(97,'HN','Honduras',0),(98,'HR','Croatia',0),(99,'HT','Haiti',0),(100,'HU','Hungary',0),(101,'ID','Indonesia',0),(102,'IE','Ireland',0),(103,'IL','Israel',0),(104,'IM','Isle of Man',0),(105,'IN','India',0),(106,'IO','British Indian Ocean Territory',0),(107,'IQ','Iraq',0),(108,'IR','Iran',0),(109,'IS','Iceland',0),(110,'IT','Italy',0),(111,'JE','Jersey',0),(112,'JM','Jamaica',0),(113,'JO','Jordan',0),(114,'JP','Japan',0),(115,'KE','Kenya',0),(116,'KG','Kyrgyzstan',0),(117,'KH','Cambodia',0),(118,'KI','Kiribati',0),(119,'KM','Comoros',0),(120,'KN','Saint Kitts and Nevis',0),(121,'KP','North Korea',0),(122,'KR','South Korea',0),(123,'KW','Kuwait',0),(124,'KY','Cayman Islands',0),(125,'KZ','Kazakhstan',0),(126,'LA','Laos',0),(127,'LB','Lebanon',0),(128,'LC','Saint Lucia',0),(129,'LI','Liechtenstein',0),(130,'LK','Sri Lanka',0),(131,'LR','Liberia',0),(132,'LS','Lesotho',0),(133,'LT','Lithuania',0),(134,'LU','Luxembourg',0),(135,'LV','Latvia',0),(136,'LY','Libyan Arab Jamahiriya',0),(137,'MA','Morocco',0),(138,'MC','Monaco',0),(139,'MD','Moldova',0),(140,'ME','Montenegro',0),(141,'MF','Saint Martin',0),(142,'MG','Madagascar',0),(143,'MH','Marshall Islands',0),(144,'MK','Macedonia',0),(145,'ML','Mali',0),(146,'MM','Myanmar',0),(147,'MN','Mongolia',0),(148,'MO','Macao',0),(149,'MP','Northern Mariana Islands',0),(150,'MQ','Martinique',0),(151,'MR','Mauritania',0),(152,'MS','Montserrat',0),(153,'MT','Malta',0),(154,'MU','Mauritius',0),(155,'MV','Maldives',0),(156,'MW','Malawi',0),(157,'MX','Mexico',0),(158,'MY','Malaysia',0),(159,'MZ','Mozambique',0),(160,'NA','Namibia',0),(161,'NC','New Caledonia',0),(162,'NE','Niger',0),(163,'NF','Norfolk Island',0),(164,'NG','Nigeria',0),(165,'NI','Nicaragua',0),(166,'NL','Netherlands',0),(167,'NO','Norway',0),(168,'NP','Nepal',0),(169,'NR','Nauru',0),(170,'NU','Niue',0),(171,'NZ','New Zealand',0),(172,'OM','Oman',0),(173,'PA','Panama',0),(174,'PE','Peru',0),(175,'PF','French Polynesia',0),(176,'PG','Papua New Guinea',0),(177,'PH','Philippines',0),(178,'PK','Pakistan',0),(179,'PL','Poland',0),(180,'PM','Saint Pierre and Miquelon',0),(181,'PN','Pitcairn Islands',0),(182,'PR','Puerto Rico',0),(183,'PS','Palestinian Territory',0),(184,'PT','Portugal',0),(185,'PW','Palau',0),(186,'PY','Paraguay',0),(187,'QA','Qatar',0),(188,'RE','RÃ©union',0),(189,'RO','Romania',0),(190,'RS','Serbia',0),(191,'RU','Russian Federation',0),(192,'RW','Rwanda',0),(193,'SA','Saudi Arabia',0),(194,'SB','Solomon Islands',0),(195,'SC','Seychelles',0),(196,'SD','Sudan',0),(197,'SE','Sweden',0),(198,'SG','Singapore',0),(199,'SH','Saint Helena',0),(200,'SI','Slovenia',0),(201,'SJ','Svalbard and Jan Mayen',0),(202,'SK','Slovakia',0),(203,'SL','Sierra Leone',0),(204,'SM','San Marino',0),(205,'SN','Senegal',0),(206,'SO','Somalia',0),(207,'SR','Suriname',0),(208,'ST','SÃ£o TomÃ© and PrÃ­ncipe',0),(209,'SV','El Salvador',0),(210,'SY','Syrian Arab Republic',0),(211,'SZ','Swaziland',0),(212,'TC','Turks and Caicos Islands',0),(213,'TD','Chad',0),(214,'TF','French Southern and Antarctic Lands',0),(215,'TG','Togo',0),(216,'TH','Thailand',0),(217,'TJ','Tajikistan',0),(218,'TK','Tokelau',0),(219,'TL','Timor-Leste',0),(220,'TM','Turkmenistan',0),(221,'TN','Tunisia',0),(222,'TO','Tonga',0),(223,'TR','Turkey',0),(224,'TT','Trinidad and Tobago',0),(225,'TV','Tuvalu',0),(226,'TW','Taiwan',0),(227,'TZ','Tanzania',0),(228,'UA','Ukraine',0),(229,'UG','Uganda',0),(230,'UM','United States Minor Outlying Islands',0),(231,'US','United States',0),(232,'UY','Uruguay',0),(233,'UZ','Uzbekistan',0),(234,'VA','Vatican',0),(235,'VC','Saint Vincent and the Grenadines',0),(236,'VE','Venezuela',0),(237,'VG','Virgin Islands, British',0),(238,'VI','Virgin Islands, U.S.',0),(239,'VN','Viet Nam',0),(240,'VU','Vanuatu',0),(241,'WF','Wallis and Futuna',0),(242,'WS','Samoa',0),(243,'YE','Yemen',0),(244,'YT','Mayotte',0),(245,'ZA','South Africa',0),(246,'ZM','Zambia',0),(247,'ZW','Zimbabwe',0);
/*!40000 ALTER TABLE `countriess` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_settings`
--

DROP TABLE IF EXISTS `email_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_settings` (
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `smtp` varchar(255) DEFAULT NULL,
  `smtp_port` varchar(255) DEFAULT NULL,
  `secure` varchar(255) DEFAULT NULL,
  `api_key` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_settings`
--

LOCK TABLES `email_settings` WRITE;
/*!40000 ALTER TABLE `email_settings` DISABLE KEYS */;
INSERT INTO `email_settings` VALUES (1,'iptvtest54@gmail.com','test1603','smtp.gmail.com','465','ssl','API_xOnaq5mkTzs2434HKXS889yqAxDyVgCc ');
/*!40000 ALTER TABLE `email_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `epg`
--

DROP TABLE IF EXISTS `epg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `epg_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `epg_path` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `epg_file` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `epg`
--

LOCK TABLES `epg` WRITE;
/*!40000 ALTER TABLE `epg` DISABLE KEYS */;
/*!40000 ALTER TABLE `epg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `epg_data`
--

DROP TABLE IF EXISTS `epg_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epg_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `epg_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `start` varchar(255) NOT NULL,
  `end` varchar(255) NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `channel_id` varchar(50) CHARACTER SET utf8 NOT NULL,
  `channel_no` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `epg_id` (`epg_id`),
  KEY `lang` (`channel_no`),
  KEY `channel_id` (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `epg_data`
--

LOCK TABLES `epg_data` WRITE;
/*!40000 ALTER TABLE `epg_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `epg_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie_vod`
--

DROP TABLE IF EXISTS `movie_vod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie_vod` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `server_id` int(11) DEFAULT '1',
  `movie_image` varchar(255) DEFAULT NULL,
  `score` varchar(255) DEFAULT NULL,
  `years` varchar(255) DEFAULT NULL,
  `movie_path` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `movie_path_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `movie_type` int(11) DEFAULT NULL,
  `movie_format` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `movie_size` varchar(255) CHARACTER SET utf8 DEFAULT '0',
  `movie_pid` int(11) DEFAULT '0',
  `download_status` int(11) DEFAULT '0',
  `download_ok` int(11) DEFAULT '0',
  `sort` int(255) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `movie_video_codec` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `movie_auido_codec` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `video_id` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie_vod`
--

LOCK TABLES `movie_vod` WRITE;
/*!40000 ALTER TABLE `movie_vod` DISABLE KEYS */;
/*!40000 ALTER TABLE `movie_vod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packets`
--

DROP TABLE IF EXISTS `packets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `packet_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `packet_type` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `duration_type` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `time` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `cost_of_credit` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category_id` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packets`
--

LOCK TABLES `packets` WRITE;
/*!40000 ALTER TABLE `packets` DISABLE KEYS */;
/*!40000 ALTER TABLE `packets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reseller`
--

DROP TABLE IF EXISTS `reseller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reseller` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `credit` varchar(255) CHARACTER SET utf8 DEFAULT '0',
  `note` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `alert_message` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `credit_number` int(11) DEFAULT '0',
  `message_number` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reseller`
--

LOCK TABLES `reseller` WRITE;
/*!40000 ALTER TABLE `reseller` DISABLE KEYS */;
INSERT INTO `reseller` VALUES (1,'demo','demo','deneme@hotmail.com','0','test',1,'credi fnish',10,1),(2,'magno','magno','audioetuning@hotmail.com','100','oplllll',1,'ola',10,0);
/*!40000 ALTER TABLE `reseller` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reseller_message`
--

DROP TABLE IF EXISTS `reseller_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reseller_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_head` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `message` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `date` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reseller_message`
--

LOCK TABLES `reseller_message` WRITE;
/*!40000 ALTER TABLE `reseller_message` DISABLE KEYS */;
INSERT INTO `reseller_message` VALUES (1,'ffffff','ola\r\n','17-02-2018 02:25:06',1);
/*!40000 ALTER TABLE `reseller_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `server`
--

DROP TABLE IF EXISTS `server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `server` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `server_ip` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `server_port` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `server_dns_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `network` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `server_status` int(11) DEFAULT '1',
  `rtmp_port` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `cron_period` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `cron_dom` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `cron_month` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `cron_mins` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `cron_dow` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `cron_time_hour` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `cron_time_min` varchar(255) DEFAULT NULL,
  `cron_status` int(11) DEFAULT '0',
  `total_reset` int(11) DEFAULT '0',
  `cron_job` varchar(255) DEFAULT NULL,
  `ssh_username` varchar(255) DEFAULT NULL,
  `ssh_password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `mysql_password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `start` int(11) DEFAULT '0',
  `start_time` varchar(255) DEFAULT NULL,
  `finish_time` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `install_status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `server`
--

LOCK TABLES `server` WRITE;
/*!40000 ALTER TABLE `server` DISABLE KEYS */;
INSERT INTO `server` VALUES (1,'Main Server','','8090','','eth0',1,'1935','','','','',NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,1),(3,'SERVER VH','','8090','144.217.65.156','eth0',1,'1935','','','','',NULL,NULL,NULL,0,0,NULL,NULL,'N8KGTfZmBQi7','1mfd231x7p',1726,0,'2018-02-23 04:03:55','2018-02-23 04:17:55',0);
/*!40000 ALTER TABLE `server` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `company_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `company_phone` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `company_email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `web` varchar(255) DEFAULT NULL,
  `logo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `licanse` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `licanse2` varchar(255) DEFAULT NULL,
  `ffmpeg_cpu` int(11) DEFAULT '0',
  `default_timezone` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `book_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `user_agent` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `auto_transcoding` int(255) DEFAULT '0',
  `http_username` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `http_password` varchar(255) DEFAULT NULL,
  `http_status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'','','','','11090640912.png','','',0,'','','',0,'proip','Bcqg6NAG',1);
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transcode_videos`
--

DROP TABLE IF EXISTS `transcode_videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transcode_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `file_path` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `video_category` int(11) DEFAULT NULL,
  `video_screen` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `video_link` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `video_bitrate` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `font_color` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `font_size` double DEFAULT '25',
  `video_time` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `logo_direction` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `subtitle` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `logo_x` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `logo_y` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `logo_width` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `logo_height` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `video_text` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `text_direction` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `text_x` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `text_y` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `live_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `test_pid` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `test_status` int(11) DEFAULT '0',
  `transcoding_start` int(11) DEFAULT '0',
  `transcoding_finish` int(11) DEFAULT '0',
  `sort` int(11) DEFAULT '0',
  `pid` int(11) DEFAULT '0',
  `video_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transcode_videos`
--

LOCK TABLES `transcode_videos` WRITE;
/*!40000 ALTER TABLE `transcode_videos` DISABLE KEYS */;
/*!40000 ALTER TABLE `transcode_videos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transcoding_manuel_profiles`
--

DROP TABLE IF EXISTS `transcoding_manuel_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transcoding_manuel_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_codec` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `auido_codec` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `preset` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `profile` varchar(255) CHARACTER SET utf8 DEFAULT '0',
  `user_agent` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `video_bitrate` int(255) DEFAULT '0',
  `auido_bitrate` int(255) DEFAULT '0',
  `max_bitrate` int(255) DEFAULT '0',
  `min_bitrate` int(255) DEFAULT '0',
  `bufsize` int(255) DEFAULT '0',
  `crf` varchar(255) DEFAULT NULL,
  `aspec` varchar(255) CHARACTER SET utf8 DEFAULT '0',
  `frame_rate` int(255) DEFAULT '0',
  `screen` varchar(255) DEFAULT '0',
  `ar` int(255) DEFAULT '0',
  `auido_channel` int(255) DEFAULT '0',
  `threads` int(255) DEFAULT '0',
  `ch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transcoding_manuel_profiles`
--

LOCK TABLES `transcoding_manuel_profiles` WRITE;
/*!40000 ALTER TABLE `transcoding_manuel_profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `transcoding_manuel_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_agent`
--

DROP TABLE IF EXISTS `user_agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_agent` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_agent`
--

LOCK TABLES `user_agent` WRITE;
/*!40000 ALTER TABLE `user_agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date_end` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `max_connection` int(11) DEFAULT '1',
  `os` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ip_adress` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `channel_category` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `note` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `ban_status` int(11) DEFAULT '0',
  `reseller_id` int(11) DEFAULT '0',
  `packet_id` int(11) DEFAULT '0',
  `create_date` datetime DEFAULT NULL,
  `cancel` int(11) DEFAULT '0',
  `cancel_message` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `test` int(255) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'ybckfql1uq','cngcqhic5m','2018-08-09 04:32:16',1,NULL,NULL,NULL,NULL,'5','',0,1,0,'2018-02-09 04:52:16',0,NULL,0),(3,'07c3e2pyp0','6zad5k6ua0','2018-08-09 04:32:53',1,NULL,NULL,NULL,NULL,'5','',0,1,0,'2018-02-09 04:52:53',0,NULL,0),(5,'magno','magno','2018-03-13 13:33',100,NULL,NULL,NULL,NULL,'5','teste',0,0,0,NULL,0,NULL,0),(6,'listacinep','ewrrewrer','2018-09-23 10:44',500,NULL,NULL,NULL,NULL,'5','lista do cine',0,0,0,NULL,0,NULL,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_category`
--

DROP TABLE IF EXISTS `video_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_category`
--

LOCK TABLES `video_category` WRITE;
/*!40000 ALTER TABLE `video_category` DISABLE KEYS */;
INSERT INTO `video_category` VALUES (1,'Turkish Videos');
/*!40000 ALTER TABLE `video_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `server_id` int(11) DEFAULT '1',
  `video_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
INSERT INTO `videos` VALUES (1,'test',1,'/home/videos/test.MKV','1'),(2,'fi',1,'/home/videos/fi.MKV','1'),(4,'test vod',1,'/home/videos/test-vod.MKV','1'),(5,'zalim',1,'/home/videos/zalim.MKV','1'),(6,'Koray',1,'/home/videos/koray.MKV','1');
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos_downloader`
--

DROP TABLE IF EXISTS `videos_downloader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos_downloader` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `video_url` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT '0',
  `download_type` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `download_start` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos_downloader`
--

LOCK TABLES `videos_downloader` WRITE;
/*!40000 ALTER TABLE `videos_downloader` DISABLE KEYS */;
INSERT INTO `videos_downloader` VALUES (5,'sesss',1,'/home/videos/sesss.MKV','ftp://proip:Bcqg6NAG@vip2.fullfilmindir.org/vip1/Bluray/720p/Mavi.Gozlu.Dev.2007.720p.DVDRip.Upscale.x264.mkv',0,1,2,2);
/*!40000 ALTER TABLE `videos_downloader` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vod_channel_time`
--

DROP TABLE IF EXISTS `vod_channel_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vod_channel_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ch_id` int(11) DEFAULT NULL,
  `video_total_time` varchar(255) DEFAULT NULL,
  `stream_close_time` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vod_channel_time`
--

LOCK TABLES `vod_channel_time` WRITE;
/*!40000 ALTER TABLE `vod_channel_time` DISABLE KEYS */;
/*!40000 ALTER TABLE `vod_channel_time` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-23 20:14:05
