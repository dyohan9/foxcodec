/*
Navicat MySQL Data Transfer

Source Server         : Fox Server
Source Server Version : 50558
Source Host           : 144.217.82.201:3306
Source Database       : iptv

Target Server Type    : MYSQL
Target Server Version : 50558
File Encoding         : 65001

Date: 2018-03-19 15:39:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for activity
-- ----------------------------
DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `stream_id` int(11) NOT NULL,
  `str_type` int(11) DEFAULT '1',
  `user_ip` varchar(255) NOT NULL,
  `os` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `pid` int(11) NOT NULL,
  `date_start` varchar(255) NOT NULL,
  `date_end` varchar(500) DEFAULT NULL,
  `container` varchar(255) DEFAULT NULL,
  `last_ts` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `date_end` (`date_end`(255))
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of activity
-- ----------------------------

-- ----------------------------
-- Table structure for administrator
-- ----------------------------
DROP TABLE IF EXISTS `administrator`;
CREATE TABLE `administrator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `create_date` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of administrator
-- ----------------------------
INSERT INTO `administrator` VALUES ('1', 'fox', 'fox', '01-12-2017 07:16:41');

-- ----------------------------
-- Table structure for alert_videos
-- ----------------------------
DROP TABLE IF EXISTS `alert_videos`;
CREATE TABLE `alert_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_stop_video` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `stop_status` int(11) DEFAULT '0',
  `member_finish_video` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `buy_status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of alert_videos
-- ----------------------------
INSERT INTO `alert_videos` VALUES ('1', '/home/fox_codec/alert_videos/leadepro2.mp4', '1', '/home/fox_codec/alert_videos/leadepro2.mp4', '1');

-- ----------------------------
-- Table structure for banned_country
-- ----------------------------
DROP TABLE IF EXISTS `banned_country`;
CREATE TABLE `banned_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ulke` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ulke_adi` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of banned_country
-- ----------------------------

-- ----------------------------
-- Table structure for banned_ip
-- ----------------------------
DROP TABLE IF EXISTS `banned_ip`;
CREATE TABLE `banned_ip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of banned_ip
-- ----------------------------

-- ----------------------------
-- Table structure for channel_category
-- ----------------------------
DROP TABLE IF EXISTS `channel_category`;
CREATE TABLE `channel_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category_abbreviation` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL COMMENT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of channel_category
-- ----------------------------
INSERT INTO `channel_category` VALUES ('1', 'Turkish Channel', 'Tr', null);

-- ----------------------------
-- Table structure for channels
-- ----------------------------
DROP TABLE IF EXISTS `channels`;
CREATE TABLE `channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `channel_logo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `channel_path_1` varchar(500) CHARACTER SET utf8mb4 NOT NULL,
  `channel_path_2` varchar(500) CHARACTER SET utf8 NOT NULL,
  `channel_path_3` varchar(500) CHARACTER SET utf8 NOT NULL,
  `server_id` int(11) NOT NULL DEFAULT '1',
  `category_id` int(11) NOT NULL,
  `transcoding` int(11) NOT NULL DEFAULT '0',
  `sort` int(255) NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `pid` int(11) NOT NULL DEFAULT '0',
  `stream_status` int(11) NOT NULL DEFAULT '0',
  `start_time` varchar(255) CHARACTER SET utf8 NOT NULL,
  `stop_time` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pid_control` int(11) NOT NULL DEFAULT '0',
  `pid_control_ok` int(11) NOT NULL DEFAULT '0',
  `control_count` int(11) NOT NULL DEFAULT '0',
  `output` varchar(255) CHARACTER SET utf8 NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `rtmp_username` varchar(255) CHARACTER SET utf8 NOT NULL,
  `rtmp_password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `hls_username` varchar(255) CHARACTER SET utf8 NOT NULL,
  `hls_password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `direct_stream` int(11) NOT NULL DEFAULT '0',
  `sleep_mod` int(255) NOT NULL DEFAULT '0',
  `channel_type` int(11) NOT NULL DEFAULT '1',
  `channel_id` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `epg_id` int(11) NOT NULL DEFAULT '0',
  `epg_lang` varchar(255) CHARACTER SET utf8 NOT NULL,
  `working_channel` int(11) NOT NULL DEFAULT '1',
  `restream` int(11) NOT NULL DEFAULT '0',
  `restream_status` int(11) NOT NULL DEFAULT '0',
  `vod_channel_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `vod_channel_url` varchar(255) CHARACTER SET utf8 NOT NULL,
  `test_pid` int(11) NOT NULL,
  `test_status` int(11) NOT NULL DEFAULT '0',
  `playlist_status` int(11) NOT NULL DEFAULT '0',
  `channel_directory` varchar(255) CHARACTER SET utf8 NOT NULL,
  `ffmpeg_command` varchar(500) CHARACTER SET utf8 DEFAULT '0',
  `token` int(11) DEFAULT NULL,
  `logo_type` int(11) NOT NULL DEFAULT '0',
  `playlist_type` int(11) NOT NULL DEFAULT '0',
  `logo_width` int(11) DEFAULT '45',
  `logo_height` int(11) DEFAULT '45',
  PRIMARY KEY (`id`),
  UNIQUE KEY `channel_id` (`id`) USING BTREE,
  KEY `server_id` (`server_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of channels
-- ----------------------------
INSERT INTO `channels` VALUES ('1', 'Test', '/channel_logo/resimyok.gif', 'http://brodilo.tv/channel.php', '', '', '1', '1', '0', '0', '0', '0', '0', '', '', '0', '0', '0', 'hls', '', '', '', '', '', '0', '0', '1', '0', '0', '', '1', '0', '0', '', '', '0', '0', '0', '', 'false', null, '0', '0', '45', '45');

-- ----------------------------
-- Table structure for channels_map
-- ----------------------------
DROP TABLE IF EXISTS `channels_map`;
CREATE TABLE `channels_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ch_id` int(11) NOT NULL,
  `map_wr` varchar(500) NOT NULL,
  `codec_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of channels_map
-- ----------------------------
INSERT INTO `channels_map` VALUES ('1', '624', '-map 0:0', 'video');
INSERT INTO `channels_map` VALUES ('2', '624', '-map 0:1', 'audio');

-- ----------------------------
-- Table structure for countriess
-- ----------------------------
DROP TABLE IF EXISTS `countriess`;
CREATE TABLE `countriess` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `AC` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of countriess
-- ----------------------------
INSERT INTO `countriess` VALUES ('1', 'AC', 'Ascension Island', '0');
INSERT INTO `countriess` VALUES ('2', 'AD', 'Andorra', '0');
INSERT INTO `countriess` VALUES ('3', 'AE', 'United Arab Emirates', '0');
INSERT INTO `countriess` VALUES ('4', 'AF', 'Afghanistan', '0');
INSERT INTO `countriess` VALUES ('5', 'AG', 'Antigua and Barbuda', '0');
INSERT INTO `countriess` VALUES ('6', 'AI', 'Anguilla', '0');
INSERT INTO `countriess` VALUES ('7', 'AL', 'Albania', '0');
INSERT INTO `countriess` VALUES ('8', 'AM', 'Armenia', '0');
INSERT INTO `countriess` VALUES ('9', 'AN', 'Netherlands Antilles', '0');
INSERT INTO `countriess` VALUES ('10', 'AO', 'Angola', '0');
INSERT INTO `countriess` VALUES ('11', 'AQ', 'Antarctica', '0');
INSERT INTO `countriess` VALUES ('12', 'AR', 'Argentina', '0');
INSERT INTO `countriess` VALUES ('13', 'AS', 'American Samoa', '0');
INSERT INTO `countriess` VALUES ('14', 'AT', 'Austria', '0');
INSERT INTO `countriess` VALUES ('15', 'AU', 'Australia', '0');
INSERT INTO `countriess` VALUES ('16', 'AW', 'Aruba', '0');
INSERT INTO `countriess` VALUES ('17', 'AX', 'Ã…land Islands', '0');
INSERT INTO `countriess` VALUES ('18', 'AZ', 'Azerbaijan', '0');
INSERT INTO `countriess` VALUES ('19', 'BA', 'Bosnia and Herzegovina', '0');
INSERT INTO `countriess` VALUES ('20', 'BB', 'Barbados', '0');
INSERT INTO `countriess` VALUES ('21', 'BD', 'Bangladesh', '0');
INSERT INTO `countriess` VALUES ('22', 'BE', 'Belgium', '0');
INSERT INTO `countriess` VALUES ('23', 'BF', 'Burkina Faso', '0');
INSERT INTO `countriess` VALUES ('24', 'BG', 'Bulgaria', '0');
INSERT INTO `countriess` VALUES ('25', 'BH', 'Bahrain', '0');
INSERT INTO `countriess` VALUES ('26', 'BI', 'Burundi', '0');
INSERT INTO `countriess` VALUES ('27', 'BJ', 'Benin', '0');
INSERT INTO `countriess` VALUES ('28', 'BL', 'Saint BarthÃ©lemy', '0');
INSERT INTO `countriess` VALUES ('29', 'BM', 'Bermuda', '0');
INSERT INTO `countriess` VALUES ('30', 'BN', 'Brunei Darussalam', '0');
INSERT INTO `countriess` VALUES ('31', 'BO', 'Bolivia', '0');
INSERT INTO `countriess` VALUES ('32', 'BR', 'Brazil', '0');
INSERT INTO `countriess` VALUES ('33', 'BS', 'Bahamas', '0');
INSERT INTO `countriess` VALUES ('34', 'BT', 'Bhutan', '0');
INSERT INTO `countriess` VALUES ('35', 'BV', 'Bouvet Island', '0');
INSERT INTO `countriess` VALUES ('36', 'BW', 'Botswana', '0');
INSERT INTO `countriess` VALUES ('37', 'BY', 'Belarus', '0');
INSERT INTO `countriess` VALUES ('38', 'BZ', 'Belize', '0');
INSERT INTO `countriess` VALUES ('39', 'CA', 'Canada', '0');
INSERT INTO `countriess` VALUES ('40', 'CC', 'Cocos (Keeling) Islands', '0');
INSERT INTO `countriess` VALUES ('41', 'CD', 'Congo, Democratic Republic of the', '0');
INSERT INTO `countriess` VALUES ('42', 'CF', 'Central African Republic', '0');
INSERT INTO `countriess` VALUES ('43', 'CG', 'Congo, Republic of the', '0');
INSERT INTO `countriess` VALUES ('44', 'CH', 'Switzerland', '0');
INSERT INTO `countriess` VALUES ('45', 'CI', 'CÃ´te d\'Ivoire', '0');
INSERT INTO `countriess` VALUES ('46', 'CK', 'Cook Islands', '0');
INSERT INTO `countriess` VALUES ('47', 'CL', 'Chile', '0');
INSERT INTO `countriess` VALUES ('48', 'CM', 'Cameroon', '0');
INSERT INTO `countriess` VALUES ('49', 'CN', 'China', '0');
INSERT INTO `countriess` VALUES ('50', 'CO', 'Colombia', '0');
INSERT INTO `countriess` VALUES ('51', 'CR', 'Costa Rica', '0');
INSERT INTO `countriess` VALUES ('52', 'CU', 'Cuba', '0');
INSERT INTO `countriess` VALUES ('53', 'CV', 'Cape Verde', '0');
INSERT INTO `countriess` VALUES ('54', 'CX', 'Christmas Island', '0');
INSERT INTO `countriess` VALUES ('55', 'CY', 'Cyprus', '0');
INSERT INTO `countriess` VALUES ('56', 'CZ', 'Czech Republic', '0');
INSERT INTO `countriess` VALUES ('57', 'DE', 'Germany', '0');
INSERT INTO `countriess` VALUES ('58', 'DJ', 'Djibouti', '0');
INSERT INTO `countriess` VALUES ('59', 'DK', 'Denmark', '0');
INSERT INTO `countriess` VALUES ('60', 'DM', 'Dominica', '0');
INSERT INTO `countriess` VALUES ('61', 'DO', 'Dominican Republic', '0');
INSERT INTO `countriess` VALUES ('62', 'DZ', 'Algeria', '0');
INSERT INTO `countriess` VALUES ('63', 'EC', 'Ecuador', '0');
INSERT INTO `countriess` VALUES ('64', 'EE', 'Estonia', '0');
INSERT INTO `countriess` VALUES ('65', 'EG', 'Egypt', '0');
INSERT INTO `countriess` VALUES ('66', 'EH', 'Western Sahara', '0');
INSERT INTO `countriess` VALUES ('67', 'ER', 'Eritrea', '0');
INSERT INTO `countriess` VALUES ('68', 'ES', 'Spain', '0');
INSERT INTO `countriess` VALUES ('69', 'ET', 'Ethiopia', '0');
INSERT INTO `countriess` VALUES ('70', 'FI', 'Finland', '0');
INSERT INTO `countriess` VALUES ('71', 'FJ', 'Fiji', '0');
INSERT INTO `countriess` VALUES ('72', 'FK', 'Falkland Islands (Malvinas)', '0');
INSERT INTO `countriess` VALUES ('73', 'FM', 'Federated States of Micronesia', '0');
INSERT INTO `countriess` VALUES ('74', 'FO', 'Faroe Islands', '0');
INSERT INTO `countriess` VALUES ('75', 'FR', 'France', '0');
INSERT INTO `countriess` VALUES ('76', 'GA', 'Gabon', '0');
INSERT INTO `countriess` VALUES ('77', 'GB', 'United Kingdom', '0');
INSERT INTO `countriess` VALUES ('78', 'GD', 'Grenada', '0');
INSERT INTO `countriess` VALUES ('79', 'GE', 'Georgia', '0');
INSERT INTO `countriess` VALUES ('80', 'GF', 'French Guiana', '0');
INSERT INTO `countriess` VALUES ('81', 'GG', 'Guernsey', '0');
INSERT INTO `countriess` VALUES ('82', 'GH', 'Ghana', '0');
INSERT INTO `countriess` VALUES ('83', 'GI', 'Gibraltar', '0');
INSERT INTO `countriess` VALUES ('84', 'GL', 'Greenland', '0');
INSERT INTO `countriess` VALUES ('85', 'GM', 'Gambia', '0');
INSERT INTO `countriess` VALUES ('86', 'GN', 'Guinea', '0');
INSERT INTO `countriess` VALUES ('87', 'GP', 'Guadeloupe', '0');
INSERT INTO `countriess` VALUES ('88', 'GQ', 'Equatorial Guinea', '0');
INSERT INTO `countriess` VALUES ('89', 'GR', 'Greece', '0');
INSERT INTO `countriess` VALUES ('90', 'GS', 'South Georgia and the South Sandwich Islands', '0');
INSERT INTO `countriess` VALUES ('91', 'GT', 'Guatemala', '0');
INSERT INTO `countriess` VALUES ('92', 'GU', 'Guam', '0');
INSERT INTO `countriess` VALUES ('93', 'GW', 'Guinea-Bissau', '0');
INSERT INTO `countriess` VALUES ('94', 'GY', 'Guyana', '0');
INSERT INTO `countriess` VALUES ('95', 'HK', 'Hong Kong', '0');
INSERT INTO `countriess` VALUES ('96', 'HM', 'Heard Island and McDonald Islands', '0');
INSERT INTO `countriess` VALUES ('97', 'HN', 'Honduras', '0');
INSERT INTO `countriess` VALUES ('98', 'HR', 'Croatia', '0');
INSERT INTO `countriess` VALUES ('99', 'HT', 'Haiti', '0');
INSERT INTO `countriess` VALUES ('100', 'HU', 'Hungary', '0');
INSERT INTO `countriess` VALUES ('101', 'ID', 'Indonesia', '0');
INSERT INTO `countriess` VALUES ('102', 'IE', 'Ireland', '0');
INSERT INTO `countriess` VALUES ('103', 'IL', 'Israel', '0');
INSERT INTO `countriess` VALUES ('104', 'IM', 'Isle of Man', '0');
INSERT INTO `countriess` VALUES ('105', 'IN', 'India', '0');
INSERT INTO `countriess` VALUES ('106', 'IO', 'British Indian Ocean Territory', '0');
INSERT INTO `countriess` VALUES ('107', 'IQ', 'Iraq', '0');
INSERT INTO `countriess` VALUES ('108', 'IR', 'Iran', '0');
INSERT INTO `countriess` VALUES ('109', 'IS', 'Iceland', '0');
INSERT INTO `countriess` VALUES ('110', 'IT', 'Italy', '0');
INSERT INTO `countriess` VALUES ('111', 'JE', 'Jersey', '0');
INSERT INTO `countriess` VALUES ('112', 'JM', 'Jamaica', '0');
INSERT INTO `countriess` VALUES ('113', 'JO', 'Jordan', '0');
INSERT INTO `countriess` VALUES ('114', 'JP', 'Japan', '0');
INSERT INTO `countriess` VALUES ('115', 'KE', 'Kenya', '0');
INSERT INTO `countriess` VALUES ('116', 'KG', 'Kyrgyzstan', '0');
INSERT INTO `countriess` VALUES ('117', 'KH', 'Cambodia', '0');
INSERT INTO `countriess` VALUES ('118', 'KI', 'Kiribati', '0');
INSERT INTO `countriess` VALUES ('119', 'KM', 'Comoros', '0');
INSERT INTO `countriess` VALUES ('120', 'KN', 'Saint Kitts and Nevis', '0');
INSERT INTO `countriess` VALUES ('121', 'KP', 'North Korea', '0');
INSERT INTO `countriess` VALUES ('122', 'KR', 'South Korea', '0');
INSERT INTO `countriess` VALUES ('123', 'KW', 'Kuwait', '0');
INSERT INTO `countriess` VALUES ('124', 'KY', 'Cayman Islands', '0');
INSERT INTO `countriess` VALUES ('125', 'KZ', 'Kazakhstan', '0');
INSERT INTO `countriess` VALUES ('126', 'LA', 'Laos', '0');
INSERT INTO `countriess` VALUES ('127', 'LB', 'Lebanon', '0');
INSERT INTO `countriess` VALUES ('128', 'LC', 'Saint Lucia', '0');
INSERT INTO `countriess` VALUES ('129', 'LI', 'Liechtenstein', '0');
INSERT INTO `countriess` VALUES ('130', 'LK', 'Sri Lanka', '0');
INSERT INTO `countriess` VALUES ('131', 'LR', 'Liberia', '0');
INSERT INTO `countriess` VALUES ('132', 'LS', 'Lesotho', '0');
INSERT INTO `countriess` VALUES ('133', 'LT', 'Lithuania', '0');
INSERT INTO `countriess` VALUES ('134', 'LU', 'Luxembourg', '0');
INSERT INTO `countriess` VALUES ('135', 'LV', 'Latvia', '0');
INSERT INTO `countriess` VALUES ('136', 'LY', 'Libyan Arab Jamahiriya', '0');
INSERT INTO `countriess` VALUES ('137', 'MA', 'Morocco', '0');
INSERT INTO `countriess` VALUES ('138', 'MC', 'Monaco', '0');
INSERT INTO `countriess` VALUES ('139', 'MD', 'Moldova', '0');
INSERT INTO `countriess` VALUES ('140', 'ME', 'Montenegro', '0');
INSERT INTO `countriess` VALUES ('141', 'MF', 'Saint Martin', '0');
INSERT INTO `countriess` VALUES ('142', 'MG', 'Madagascar', '0');
INSERT INTO `countriess` VALUES ('143', 'MH', 'Marshall Islands', '0');
INSERT INTO `countriess` VALUES ('144', 'MK', 'Macedonia', '0');
INSERT INTO `countriess` VALUES ('145', 'ML', 'Mali', '0');
INSERT INTO `countriess` VALUES ('146', 'MM', 'Myanmar', '0');
INSERT INTO `countriess` VALUES ('147', 'MN', 'Mongolia', '0');
INSERT INTO `countriess` VALUES ('148', 'MO', 'Macao', '0');
INSERT INTO `countriess` VALUES ('149', 'MP', 'Northern Mariana Islands', '0');
INSERT INTO `countriess` VALUES ('150', 'MQ', 'Martinique', '0');
INSERT INTO `countriess` VALUES ('151', 'MR', 'Mauritania', '0');
INSERT INTO `countriess` VALUES ('152', 'MS', 'Montserrat', '0');
INSERT INTO `countriess` VALUES ('153', 'MT', 'Malta', '0');
INSERT INTO `countriess` VALUES ('154', 'MU', 'Mauritius', '0');
INSERT INTO `countriess` VALUES ('155', 'MV', 'Maldives', '0');
INSERT INTO `countriess` VALUES ('156', 'MW', 'Malawi', '0');
INSERT INTO `countriess` VALUES ('157', 'MX', 'Mexico', '0');
INSERT INTO `countriess` VALUES ('158', 'MY', 'Malaysia', '0');
INSERT INTO `countriess` VALUES ('159', 'MZ', 'Mozambique', '0');
INSERT INTO `countriess` VALUES ('160', 'NA', 'Namibia', '0');
INSERT INTO `countriess` VALUES ('161', 'NC', 'New Caledonia', '0');
INSERT INTO `countriess` VALUES ('162', 'NE', 'Niger', '0');
INSERT INTO `countriess` VALUES ('163', 'NF', 'Norfolk Island', '0');
INSERT INTO `countriess` VALUES ('164', 'NG', 'Nigeria', '0');
INSERT INTO `countriess` VALUES ('165', 'NI', 'Nicaragua', '0');
INSERT INTO `countriess` VALUES ('166', 'NL', 'Netherlands', '0');
INSERT INTO `countriess` VALUES ('167', 'NO', 'Norway', '0');
INSERT INTO `countriess` VALUES ('168', 'NP', 'Nepal', '0');
INSERT INTO `countriess` VALUES ('169', 'NR', 'Nauru', '0');
INSERT INTO `countriess` VALUES ('170', 'NU', 'Niue', '0');
INSERT INTO `countriess` VALUES ('171', 'NZ', 'New Zealand', '0');
INSERT INTO `countriess` VALUES ('172', 'OM', 'Oman', '0');
INSERT INTO `countriess` VALUES ('173', 'PA', 'Panama', '0');
INSERT INTO `countriess` VALUES ('174', 'PE', 'Peru', '0');
INSERT INTO `countriess` VALUES ('175', 'PF', 'French Polynesia', '0');
INSERT INTO `countriess` VALUES ('176', 'PG', 'Papua New Guinea', '0');
INSERT INTO `countriess` VALUES ('177', 'PH', 'Philippines', '0');
INSERT INTO `countriess` VALUES ('178', 'PK', 'Pakistan', '0');
INSERT INTO `countriess` VALUES ('179', 'PL', 'Poland', '0');
INSERT INTO `countriess` VALUES ('180', 'PM', 'Saint Pierre and Miquelon', '0');
INSERT INTO `countriess` VALUES ('181', 'PN', 'Pitcairn Islands', '0');
INSERT INTO `countriess` VALUES ('182', 'PR', 'Puerto Rico', '0');
INSERT INTO `countriess` VALUES ('183', 'PS', 'Palestinian Territory', '0');
INSERT INTO `countriess` VALUES ('184', 'PT', 'Portugal', '0');
INSERT INTO `countriess` VALUES ('185', 'PW', 'Palau', '0');
INSERT INTO `countriess` VALUES ('186', 'PY', 'Paraguay', '0');
INSERT INTO `countriess` VALUES ('187', 'QA', 'Qatar', '0');
INSERT INTO `countriess` VALUES ('188', 'RE', 'RÃ©union', '0');
INSERT INTO `countriess` VALUES ('189', 'RO', 'Romania', '0');
INSERT INTO `countriess` VALUES ('190', 'RS', 'Serbia', '0');
INSERT INTO `countriess` VALUES ('191', 'RU', 'Russian Federation', '0');
INSERT INTO `countriess` VALUES ('192', 'RW', 'Rwanda', '0');
INSERT INTO `countriess` VALUES ('193', 'SA', 'Saudi Arabia', '0');
INSERT INTO `countriess` VALUES ('194', 'SB', 'Solomon Islands', '0');
INSERT INTO `countriess` VALUES ('195', 'SC', 'Seychelles', '0');
INSERT INTO `countriess` VALUES ('196', 'SD', 'Sudan', '0');
INSERT INTO `countriess` VALUES ('197', 'SE', 'Sweden', '0');
INSERT INTO `countriess` VALUES ('198', 'SG', 'Singapore', '0');
INSERT INTO `countriess` VALUES ('199', 'SH', 'Saint Helena', '0');
INSERT INTO `countriess` VALUES ('200', 'SI', 'Slovenia', '0');
INSERT INTO `countriess` VALUES ('201', 'SJ', 'Svalbard and Jan Mayen', '0');
INSERT INTO `countriess` VALUES ('202', 'SK', 'Slovakia', '0');
INSERT INTO `countriess` VALUES ('203', 'SL', 'Sierra Leone', '0');
INSERT INTO `countriess` VALUES ('204', 'SM', 'San Marino', '0');
INSERT INTO `countriess` VALUES ('205', 'SN', 'Senegal', '0');
INSERT INTO `countriess` VALUES ('206', 'SO', 'Somalia', '0');
INSERT INTO `countriess` VALUES ('207', 'SR', 'Suriname', '0');
INSERT INTO `countriess` VALUES ('208', 'ST', 'SÃ£o TomÃ© and PrÃ­ncipe', '0');
INSERT INTO `countriess` VALUES ('209', 'SV', 'El Salvador', '0');
INSERT INTO `countriess` VALUES ('210', 'SY', 'Syrian Arab Republic', '0');
INSERT INTO `countriess` VALUES ('211', 'SZ', 'Swaziland', '0');
INSERT INTO `countriess` VALUES ('212', 'TC', 'Turks and Caicos Islands', '0');
INSERT INTO `countriess` VALUES ('213', 'TD', 'Chad', '0');
INSERT INTO `countriess` VALUES ('214', 'TF', 'French Southern and Antarctic Lands', '0');
INSERT INTO `countriess` VALUES ('215', 'TG', 'Togo', '0');
INSERT INTO `countriess` VALUES ('216', 'TH', 'Thailand', '0');
INSERT INTO `countriess` VALUES ('217', 'TJ', 'Tajikistan', '0');
INSERT INTO `countriess` VALUES ('218', 'TK', 'Tokelau', '0');
INSERT INTO `countriess` VALUES ('219', 'TL', 'Timor-Leste', '0');
INSERT INTO `countriess` VALUES ('220', 'TM', 'Turkmenistan', '0');
INSERT INTO `countriess` VALUES ('221', 'TN', 'Tunisia', '0');
INSERT INTO `countriess` VALUES ('222', 'TO', 'Tonga', '0');
INSERT INTO `countriess` VALUES ('223', 'TR', 'Turkey', '0');
INSERT INTO `countriess` VALUES ('224', 'TT', 'Trinidad and Tobago', '0');
INSERT INTO `countriess` VALUES ('225', 'TV', 'Tuvalu', '0');
INSERT INTO `countriess` VALUES ('226', 'TW', 'Taiwan', '0');
INSERT INTO `countriess` VALUES ('227', 'TZ', 'Tanzania', '0');
INSERT INTO `countriess` VALUES ('228', 'UA', 'Ukraine', '0');
INSERT INTO `countriess` VALUES ('229', 'UG', 'Uganda', '0');
INSERT INTO `countriess` VALUES ('230', 'UM', 'United States Minor Outlying Islands', '0');
INSERT INTO `countriess` VALUES ('231', 'US', 'United States', '0');
INSERT INTO `countriess` VALUES ('232', 'UY', 'Uruguay', '0');
INSERT INTO `countriess` VALUES ('233', 'UZ', 'Uzbekistan', '0');
INSERT INTO `countriess` VALUES ('234', 'VA', 'Vatican', '0');
INSERT INTO `countriess` VALUES ('235', 'VC', 'Saint Vincent and the Grenadines', '0');
INSERT INTO `countriess` VALUES ('236', 'VE', 'Venezuela', '0');
INSERT INTO `countriess` VALUES ('237', 'VG', 'Virgin Islands, British', '0');
INSERT INTO `countriess` VALUES ('238', 'VI', 'Virgin Islands, U.S.', '0');
INSERT INTO `countriess` VALUES ('239', 'VN', 'Viet Nam', '0');
INSERT INTO `countriess` VALUES ('240', 'VU', 'Vanuatu', '0');
INSERT INTO `countriess` VALUES ('241', 'WF', 'Wallis and Futuna', '0');
INSERT INTO `countriess` VALUES ('242', 'WS', 'Samoa', '0');
INSERT INTO `countriess` VALUES ('243', 'YE', 'Yemen', '0');
INSERT INTO `countriess` VALUES ('244', 'YT', 'Mayotte', '0');
INSERT INTO `countriess` VALUES ('245', 'ZA', 'South Africa', '0');
INSERT INTO `countriess` VALUES ('246', 'ZM', 'Zambia', '0');
INSERT INTO `countriess` VALUES ('247', 'ZW', 'Zimbabwe', '0');

-- ----------------------------
-- Table structure for email_settings
-- ----------------------------
DROP TABLE IF EXISTS `email_settings`;
CREATE TABLE `email_settings` (
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `smtp` varchar(255) DEFAULT NULL,
  `smtp_port` varchar(255) DEFAULT NULL,
  `secure` varchar(255) DEFAULT NULL,
  `api_key` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of email_settings
-- ----------------------------
INSERT INTO `email_settings` VALUES ('1', 'tvsunucusu@gmail.com', 'hakan1603', 'smtp.gmail.com', '587', 'tls', 'API_xOnaq5mkTzs2434HKXS889yqAxDyVgCc ');

-- ----------------------------
-- Table structure for epg
-- ----------------------------
DROP TABLE IF EXISTS `epg`;
CREATE TABLE `epg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `epg_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `epg_path` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `epg_file` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for epg_data
-- ----------------------------
DROP TABLE IF EXISTS `epg_data`;
CREATE TABLE `epg_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `epg_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `start` varchar(255) NOT NULL,
  `end` varchar(255) NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `channel_id` varchar(50) CHARACTER SET utf8 NOT NULL,
  `channel_no` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `epg_id` (`epg_id`),
  KEY `lang` (`channel_no`),
  KEY `channel_id` (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of epg_data
-- ----------------------------

-- ----------------------------
-- Table structure for movie_vod
-- ----------------------------
DROP TABLE IF EXISTS `movie_vod`;
CREATE TABLE `movie_vod` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `server_id` int(11) DEFAULT '1',
  `video_category_id` int(11) DEFAULT NULL,
  `movie_image` varchar(255) DEFAULT NULL,
  `score` varchar(255) DEFAULT NULL,
  `years` varchar(255) DEFAULT NULL,
  `movie_path` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `movie_path_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `movie_type` int(11) DEFAULT NULL,
  `movie_format` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `movie_size` varchar(255) CHARACTER SET utf8 DEFAULT '0',
  `movie_pid` int(11) DEFAULT '0',
  `download_status` int(11) DEFAULT '0',
  `download_ok` int(11) DEFAULT '0',
  `sort` int(255) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `movie_video_codec` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `movie_auido_codec` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `video_id` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of movie_vod
-- ----------------------------

-- ----------------------------
-- Table structure for no_logo_playlist
-- ----------------------------
DROP TABLE IF EXISTS `no_logo_playlist`;
CREATE TABLE `no_logo_playlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `file_path` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `video_time` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `video_id` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT '1',
  `types` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of no_logo_playlist
-- ----------------------------

-- ----------------------------
-- Table structure for packets
-- ----------------------------
DROP TABLE IF EXISTS `packets`;
CREATE TABLE `packets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `packet_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `packet_type` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `duration_type` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `time` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `cost_of_credit` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category_id` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of packets
-- ----------------------------

-- ----------------------------
-- Table structure for playlist_date
-- ----------------------------
DROP TABLE IF EXISTS `playlist_date`;
CREATE TABLE `playlist_date` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playlist_name` varchar(255) DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `playlist_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of playlist_date
-- ----------------------------

-- ----------------------------
-- Table structure for reseller
-- ----------------------------
DROP TABLE IF EXISTS `reseller`;
CREATE TABLE `reseller` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `credit` varchar(255) CHARACTER SET utf8 DEFAULT '0',
  `note` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `alert_message` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `credit_number` int(11) DEFAULT '0',
  `message_number` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of reseller
-- ----------------------------

-- ----------------------------
-- Table structure for reseller_message
-- ----------------------------
DROP TABLE IF EXISTS `reseller_message`;
CREATE TABLE `reseller_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_head` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `message` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `date` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of reseller_message
-- ----------------------------

-- ----------------------------
-- Table structure for server
-- ----------------------------
DROP TABLE IF EXISTS `server`;
CREATE TABLE `server` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `server_ip` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `server_port` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `server_dns_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `network` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `server_status` int(11) DEFAULT '1',
  `rtmp_port` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `cron_period` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `cron_dom` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `cron_month` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `cron_mins` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `cron_dow` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `cron_time_hour` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `cron_time_min` varchar(255) DEFAULT NULL,
  `cron_status` int(11) DEFAULT '0',
  `total_reset` int(11) DEFAULT '0',
  `cron_job` varchar(255) DEFAULT NULL,
  `ssh_username` varchar(255) DEFAULT NULL,
  `ssh_password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `mysql_password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `start` int(11) DEFAULT '0',
  `start_time` varchar(255) DEFAULT NULL,
  `finish_time` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `install_status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of server
-- ----------------------------
INSERT INTO `server` VALUES ('1', 'Main Server', '144.217.82.201', '9080', '144.217.82.201', 'eth0', '1', '1935', '', '', '', '', null, null, null, '0', '0', null, null, null, null, null, '0', null, null, '1');
INSERT INTO `server` VALUES ('3', 'Balance Server', '144.217.165.19', '8090', '144.217.165.19', 'eth0', '1', '1935', '', '', '', '', null, null, null, '0', '0', null, null, 'pzEmpHH1', 'db9o8wbxr7', '1757', '0', '2018-03-06 00:15:00', '2018-03-06 00:29:00', '0');

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `company_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `company_phone` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `company_email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `web` varchar(255) DEFAULT NULL,
  `logo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `licanse` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `licanse2` varchar(255) DEFAULT NULL,
  `ffmpeg_cpu` int(11) DEFAULT '0',
  `default_timezone` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `book_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `user_agent` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `auto_transcoding` int(255) DEFAULT '0',
  `http_username` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `http_password` varchar(255) DEFAULT NULL,
  `http_status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES ('1', 'Fox Codec IPTV', '+90 505 055 22 666', 'info@foxcodec.com', 'www.foxcodec.com', '10414847250.png', 'FOX30E99-8A138-B2B07-5B193-712EF-F0E5F-3280B-C655B-2018', 'FOX85-3B15F-E11B1-9A2CE-26CA2-36416-390D8-2018', '0', 'Europe/Athens', 'fox', '', '0', 'nexa', '852606', '0');

-- ----------------------------
-- Table structure for transcode_videos
-- ----------------------------
DROP TABLE IF EXISTS `transcode_videos`;
CREATE TABLE `transcode_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `file_path` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `video_category` int(11) DEFAULT NULL,
  `video_screen` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `video_link` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `video_bitrate` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `font_color` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `font_size` double DEFAULT '25',
  `video_time` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `logo_direction` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `subtitle` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `logo_x` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `logo_y` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `frame_rate` varchar(255) DEFAULT NULL,
  `opacit` int(100) DEFAULT NULL,
  `text_opacit` int(11) DEFAULT '0',
  `logo_width` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `logo_height` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `video_text` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `text_direction` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `text_x` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `text_y` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `live_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `test_pid` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `test_status` int(11) DEFAULT '0',
  `transcoding_start` int(11) DEFAULT '0',
  `transcoding_finish` int(11) DEFAULT '0',
  `sort` int(11) DEFAULT '1',
  `trs_sort` int(11) DEFAULT '1',
  `pid` int(11) DEFAULT '0',
  `video_id` int(11) DEFAULT NULL,
  `types` int(11) DEFAULT '1',
  `map` int(11) DEFAULT '1',
  `sub_title_map` int(255) DEFAULT '0',
  `video_codec` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of transcode_videos
-- ----------------------------

-- ----------------------------
-- Table structure for transcoding_channels
-- ----------------------------
DROP TABLE IF EXISTS `transcoding_channels`;
CREATE TABLE `transcoding_channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) NOT NULL,
  `trs_profil_id` int(11) NOT NULL,
  `frame_rate` char(255) NOT NULL DEFAULT '0',
  `logo_status` int(11) NOT NULL DEFAULT '0',
  `text_status` int(11) NOT NULL DEFAULT '0',
  `bitrate` int(11) NOT NULL DEFAULT '0',
  `min_bitrate` int(11) NOT NULL DEFAULT '0',
  `max_bitrate` int(11) NOT NULL DEFAULT '0',
  `bufsize` int(11) NOT NULL DEFAULT '0',
  `logo_direction` varchar(255) CHARACTER SET utf8 NOT NULL,
  `logo_x` int(11) NOT NULL DEFAULT '0',
  `logo_y` int(11) NOT NULL DEFAULT '0',
  `logo_width` int(11) NOT NULL DEFAULT '0',
  `logo_height` int(11) NOT NULL DEFAULT '0',
  `text_direction` int(11) NOT NULL DEFAULT '0',
  `text_x` int(11) NOT NULL DEFAULT '0',
  `text_y` int(11) NOT NULL,
  `font_size` int(11) NOT NULL,
  `font_color` varchar(255) CHARACTER SET utf8 NOT NULL,
  `text` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of transcoding_channels
-- ----------------------------

-- ----------------------------
-- Table structure for transcoding_profil
-- ----------------------------
DROP TABLE IF EXISTS `transcoding_profil`;
CREATE TABLE `transcoding_profil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profil_name` varchar(255) DEFAULT NULL,
  `video_codec` varchar(255) DEFAULT NULL,
  `auido_codec` varchar(255) DEFAULT NULL,
  `screen` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of transcoding_profil
-- ----------------------------
INSERT INTO `transcoding_profil` VALUES ('1', 'AAC-64KBPS SD-720x576 - H264', '-c:v libx264', '-acodec aac -ab 64k -ar 44100 -ac 2', '-s:v 720x576');
INSERT INTO `transcoding_profil` VALUES ('2', 'AAC-128KBPS SD-720x576 - H264', '-c:v libx264', '-acodec aac -ab 128k -ar 44100 -ac 2', '-s:v 720x576');
INSERT INTO `transcoding_profil` VALUES ('3', 'AAC-64KBPS HD-1280x720 - H264', '-c:v libx264', '-acodec aac -ab 64k -ar 44100 -ac 2', '-s:v 1280x720');
INSERT INTO `transcoding_profil` VALUES ('4', 'AAC-128KBPS HD-1280x720 - H264', '-c:v libx264', '-acodec aac -ab 128k -ar 44100 -ac 2', '-s:v 1280x720');
INSERT INTO `transcoding_profil` VALUES ('5', 'AAC-64KBPS UHD-1920x1080 - H264', '-c:v libx264', '-acodec aac -ab 64k -ar 44100 -ac 2', '-s:v 1920x1080');
INSERT INTO `transcoding_profil` VALUES ('6', 'AAC-128KBPS UHD-1920x1080 - H264', '-c:v libx264', '-acodec aac -ab 128k -ar 44100 -ac 2', '-s:v 1920x1080');

-- ----------------------------
-- Table structure for transcoding_settings
-- ----------------------------
DROP TABLE IF EXISTS `transcoding_settings`;
CREATE TABLE `transcoding_settings` (
  `id` int(11) NOT NULL,
  `transcoding_limit` int(11) DEFAULT '1',
  `cpu_limit` varchar(255) DEFAULT NULL,
  `ram_limit` varchar(255) DEFAULT NULL,
  `cpu_transcode_limit` int(11) DEFAULT '1',
  `test_time` int(11) DEFAULT '20',
  `download_limit` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of transcoding_settings
-- ----------------------------
INSERT INTO `transcoding_settings` VALUES ('0', '1', '100', '100', '1', '120', '80000');

-- ----------------------------
-- Table structure for user_agent
-- ----------------------------
DROP TABLE IF EXISTS `user_agent`;
CREATE TABLE `user_agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_agent` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_agent
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date_end` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `max_connection` int(11) DEFAULT '1',
  `os` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ip_adress` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `channel_category` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `note` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `ban_status` int(11) DEFAULT '0',
  `reseller_id` int(11) DEFAULT '0',
  `packet_id` int(11) DEFAULT '0',
  `create_date` datetime DEFAULT NULL,
  `cancel` int(11) DEFAULT '0',
  `cancel_message` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `test` int(255) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'test', 'test', '0000-00-00 00:00:00', '1', null, null, null, null, '1', '', '0', '0', '0', null, '0', null, '0');

-- ----------------------------
-- Table structure for video_break_videos
-- ----------------------------
DROP TABLE IF EXISTS `video_break_videos`;
CREATE TABLE `video_break_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_name` varchar(255) DEFAULT NULL,
  `video_url` varchar(255) DEFAULT NULL,
  `video_link` varchar(255) DEFAULT NULL,
  `video_time` varchar(255) DEFAULT NULL,
  `video_pid` int(11) DEFAULT '0',
  `transcoding_start` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of video_break_videos
-- ----------------------------
INSERT INTO `video_break_videos` VALUES ('6', 'relam.mp4.relam', '/home/videos//relammp457987.mp4', '/home/fox_codec/upload/3970860.ts', '41.647000\n', '0', '2', '2');

-- ----------------------------
-- Table structure for video_category
-- ----------------------------
DROP TABLE IF EXISTS `video_category`;
CREATE TABLE `video_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of video_category
-- ----------------------------
INSERT INTO `video_category` VALUES ('1', 'Turkish Videos');

-- ----------------------------
-- Table structure for videos
-- ----------------------------
DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `server_id` int(11) DEFAULT '1',
  `video_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category_id` varchar(255) DEFAULT NULL,
  `sira` int(255) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of videos
-- ----------------------------

-- ----------------------------
-- Table structure for videos_downloader
-- ----------------------------
DROP TABLE IF EXISTS `videos_downloader`;
CREATE TABLE `videos_downloader` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `video_url` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT '0',
  `download_type` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `download_start` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of videos_downloader
-- ----------------------------

-- ----------------------------
-- Table structure for vod_channel_time
-- ----------------------------
DROP TABLE IF EXISTS `vod_channel_time`;
CREATE TABLE `vod_channel_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ch_id` int(11) DEFAULT NULL,
  `video_total_time` varchar(255) DEFAULT NULL,
  `stream_close_time` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vod_channel_time
-- ----------------------------
INSERT INTO `vod_channel_time` VALUES ('1', '1520', null, null);
