#!/bin/bash

# FUNCTION: Ubuntu 14.04 Check
distro(){
if [ -f /etc/lsb-release ]; then
    . /etc/lsb-release
        if [ $DISTRIB_ID == Ubuntu ]; then
            if [ $DISTRIB_RELEASE != "14.04" ]; then
                error
            fi
        else
            error
        fi
fi
}

# FUNCTION: ERROR
error(){
    sleep 2
    echo -ne '\n'"--PROBLEM!--"
    echo -ne '\n'"Support: https://foxcodec.com" '\n'
exit
}
 
echo "Your System is UBUNTU $OS - $VER"
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "[+] INSTALLING PACKET PLEASE WAIT"
read -p "Please enter a MySQL root password:" mysqlpassword
echo > /home/iptv.log
    apt-get autoremove
	apt-get purge php.*
	apt-get remove --purge mysql*
	apt-get remove --purge nginx*
	apt-get -y update
	apt-get -y install psmisc
	killall -9 apache2
	killall -9 nginx
	killall -9 php-fpm
	apt-get -y install git
	apt-get -y install htop
	apt-get -y install unzip
	apt-get -y remove apache2
	apt-get -y install unrar
	apt-get -y install build-essential libpcre3 libpcre3-dev libssl-dev make git
    apt-get install -y --force-yes git
    apt-get install -y --force-yes install php5-cli curl
    apt-get install -y --force-yes libxml2-dev
    apt-get install -y --force-yes libbz2-dev
    apt-get install -y --force-yes curl
    apt-get install -y --force-yes libcurl4-openssl-dev
    apt-get install -y --force-yes libmcrypt-dev
    apt-get install -y --force-yes libmhash2
    apt-get install -y --force-yes libmhash-dev
    apt-get install -y --force-yes libpcre3
    apt-get install -y --force-yes libpcre3-dev
    apt-get install -y --force-yes make
    apt-get install -y --force-yes build-essential
    apt-get install -y --force-yes libxslt1-dev git
    apt-get install -y --force-yes libssl-dev
    apt-get install -y --force-yes git
    apt-get install -y --force-yes php5
    apt-get install -y --force-yes unzip
    apt-get install -y --force-yes python-software-properties
    apt-get install -y --force-yes libpopt0
    apt-get install -y --force-yes libpq-dev
    apt-get install -y --force-yes libpq5
    apt-get install -y --force-yes libpspell-dev
    apt-get install -y --force-yes libpthread-stubs0-dev
    apt-get install -y --force-yes libpython-stdlib
    apt-get install -y --force-yes libqdbm-dev
    apt-get install -y --force-yes libqdbm14
    apt-get install -y --force-yes libquadmath0
    apt-get install -y --force-yes librecode-dev
    apt-get install -y --force-yes librecode0
    apt-get install -y --force-yes librtmp-dev
    apt-get install -y --force-yes librtmp0
    apt-get install -y --force-yes libsasl2-dev
    apt-get install -y --force-yes libsasl2-modules
    apt-get install -y --force-yes libsctp-dev
    apt-get install -y --force-yes libsctp1
    apt-get install -y --force-yes libsensors4
    apt-get install -y --force-yes libsensors4-dev
    apt-get install -y --force-yes libsm-dev
    apt-get install -y --force-yes libsm6
    apt-get install -y --force-yes libsnmp-base
    apt-get install -y --force-yes libsnmp-dev
    apt-get install -y --force-yes libsnmp-perl
    apt-get install -y --force-yes libsnmp30
    apt-get install -y --force-yes libsqlite3-dev
    apt-get install -y --force-yes libssh2-1
    apt-get install -y --force-yes libssh2-1-dev
    apt-get install -y --force-yes libssl-dev
    apt-get install -y --force-yes libstdc++-4.8-dev
    apt-get install -y --force-yes libstdc++6-4.7-dev
    apt-get install -y --force-yes libsybdb5
    apt-get install -y --force-yes libtasn1-3-dev
    apt-get install -y --force-yes libtasn1-6-dev
    apt-get install -y --force-yes libterm-readkey-perl
    apt-get install -y --force-yes libtidy-0.99-0
    apt-get install -y --force-yes libtidy-dev
    apt-get install -y --force-yes libtiff5
    apt-get install -y --force-yes libtiff5-dev
    apt-get install -y --force-yes libtiffxx5
    apt-get install -y --force-yes libtimedate-perl
    apt-get install -y --force-yes libtinfo-dev
    apt-get install -y --force-yes libtool
    apt-get install -y --force-yes libtsan0
    apt-get install -y --force-yes libunistring0
    apt-get install -y --force-yes libvpx-dev
    apt-get install -y --force-yes libvpx1
    apt-get install -y --force-yes libwrap0-dev
    apt-get install -y --force-yes libx11-6
    apt-get install -y --force-yes libx11-data
    apt-get install -y --force-yes libx11-dev
    apt-get install -y --force-yes libxau-dev
    apt-get install -y --force-yes libxau6
    apt-get install -y --force-yes libxcb1
    apt-get install -y --force-yes libxcb1-dev
    apt-get install -y --force-yes libxdmcp-dev
    apt-get install -y --force-yes libxdmcp6
    apt-get install -y --force-yes libxml2
    apt-get install -y --force-yes libxml2-dev
    apt-get install -y --force-yes libxmltok1
    apt-get install -y --force-yes libxmltok1-dev
    apt-get install -y --force-yes libxpm-dev
    apt-get install -y --force-yes libxpm4
    apt-get install -y --force-yes libxslt1-dev
    apt-get install -y --force-yes libxslt1.1
    apt-get install -y --force-yes libxt-dev
    apt-get install -y --force-yes libxt6
    apt-get install -y --force-yes linux-libc-dev
    apt-get install -y --force-yes m4
    apt-get install -y --force-yes make
    apt-get install -y --force-yes man-db
    apt-get install -y --force-yes netcat-openbsd
    apt-get install -y --force-yes odbcinst1debian2
    apt-get install -y --force-yes openssl
    apt-get install -y --force-yes patch
    apt-get install -y --force-yes pkg-config
    apt-get install -y --force-yes po-debconf
    apt-get install -y --force-yes python
    apt-get install -y --force-yes python-minimal
    apt-get install -y --force-yes python2.7
    apt-get install -y --force-yes python2.7-minimal
    apt-get install -y --force-yes re2c
    apt-get install -y --force-yes unixodbc
    apt-get install -y --force-yes unixodbc-dev
    apt-get install -y --force-yes uuid-dev
    apt-get install -y --force-yes x11-common
    apt-get install -y --force-yes x11proto-core-dev
    apt-get install -y --force-yes x11proto-input-dev
    apt-get install -y --force-yes x11proto-kb-dev
    apt-get install -y --force-yes xorg-sgml-doctools
    apt-get install -y --force-yes libjpeg8
    apt-get install -y --force-yes xtrans-dev
    apt-get install -y --force-yes zlib1g-dev
	apt-get -y install libfcgi-dev libfcgi0ldbl libjpeg62-dbg libmcrypt-dev libssl-dev libc-client2007e libc-client2007e-dev libxml2-dev libbz2-dev libcurl4-openssl-dev libjpeg-dev libpng12-dev libfreetype6-dev libkrb5-dev libpq-dev libxml2-dev libxslt1-dev
    sudo apt-get -y install php5-cgi php5-mysql php5-curl php5-gd php5-idn php-pear php5-imagick php5-imap php5-mcrypt php5-memcache php5-mhash php5-pspell php5-recode php5-sqlite php5-tidy php5-xmlrpc php5-xsl
	sudo apt-get install vlc vlc-plugin-* -y && sudo apt-get install vlc browser-plugin-vlc -y
	
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "[+] INSTALLING FOLDER PLEASE WAIT"
mkdir /home/log/
mkdir /home/logs/
mkdir /home/tmp/
mkdir /home/crons/
mkdir /home/fox_codec/
mkdir /home/fox_codec/config/
mkdir /home/nginx/
mkdir /home/youtube/
mkdir /home/movies/
mkdir /home/server_movies/
mkdir /home/live/
mkdir /home/transcode_video/
mkdir /home/channel_list/
mkdir /home/videos/

chown www-data /home/log/
chown www-data /home/logs/
chown www-data /home/tmp/
chown www-data /home/fox_codec/
chown www-data /home/fox_codec/config/
chown www-data /home/nginx 
chown www-data /home/youtube
chown www-data /home/crons
chown www-data /home/movies
chown www-data /home/live
chown www-data /home/server_movies
chown www-data /home/transcode_video
chown www-data /home/channel_list
chown www-data /home/videos

chmod 777 /home/tmp/
chmod 777 /home/log/
chmod 777 /home/logs/
chmod 777 /home/live/
chmod 777 /home/movies/
chmod 777 /home/server_movies/
chmod 777 /home/transcode_video/
chmod 777 /home/fox_codec/config/
chmod 777 /home/channel_list/
chmod 777 /home/videos/

echo "[+] INSTALLING SUDO PLEASE WAIT"
apt-get -y install sudo
echo 'www-data ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

echo "[+] INSTALLING MYSQL SERVER PLEASE WAIT"
echo mysql-server mysql-server/root_password password $mysqlpassword | debconf-set-selections > /dev/null 2>&1
echo mysql-server mysql-server/root_password_again password $mysqlpassword | debconf-set-selections > /dev/null 2>&1
apt-get -y install mysql-server > /dev/null 2>&1

echo "[+] IMPORTING SQL FILE PLEASE WAIT"
wget -q http://licenseiptv.bestmice.top/fox_codec/fx/fox_codec.sql > /dev/null 2>&1
mysql -uroot -p$mysqlpassword -e "CREATE DATABASE iptv" > /dev/null 2>&1
mysql -uroot -p$mysqlpassword -e "grant all privileges on iptv.* to 'blc'@'%' identified by '$mysqlpassword'"
mysql -uroot -p$mysqlpassword iptv < fox_codec.sql > /dev/null 2>&1
rm -f fox_codec.sql > /dev/null 2>&1

echo "[+] INSTALLING CONFIG FILE PLEASE WAIT"
wget -q http://licenseiptv.bestmice.top/fox_codec/fx/config.zip > /dev/null 2>&1
unzip -o config.zip -d /home/fox_codec/config/ > /dev/null 2>&1
rm -f config.zip > /dev/null 2>&1
echo 'define("DB_HOST", "localhost");' >> /home/fox_codec/config/config.php
echo 'define("DB_NAME", "iptv");' >> /home/fox_codec/config/config.php
echo 'define("DB_USER", "root");' >> /home/fox_codec/config/config.php
a='define("DB_PASS", "'
b='");'
c=$a$mysqlpassword$b
echo $c >> /home/fox_codec/config/config.php

echo "[+] STALKER CONFIG FILE PLEASE WAIT"
wget -q http://licenseiptv.bestmice.top/fox_codec/fx/stalker_config.zip > /dev/null 2>&1
unzip -o stalker_config.zip -d /home/fox_codec/config/ > /dev/null 2>&1
rm -f stalker_config.zip > /dev/null 2>&1
echo 'define("DB_HOST", "localhost");' >> /home/fox_codec/config/stalker_config.php
echo 'define("DB_NAME", "stalker_db");' >> /home/fox_codec/config/stalker_config.php
echo 'define("DB_USER", "root");' >> /home/fox_codec/config/stalker_config.php
a='define("DB_PASS", "'
b='");'
c=$a$mysqlpassword$b
echo $c >> /home/fox_codec/config/stalker_config.php

echo "[+] INSTALLING FFMPEG PLEASE WAIT"
wget http://licenseiptv.bestmice.top/fox_codec/fx/ffmpeg.zip -O /home/tmp/ffmpeg.zip > /dev/null 2>&1
unzip -o /home/tmp/ffmpeg.zip -d /home/ffmpeg > /dev/null 2>&1
rm -f /home/tmp/ffmpeg.zip > /dev/null 2>&1
wget http://licenseiptv.bestmice.top/fox_codec/fx/cron.zip -O /home/tmp/cron.zip > /dev/null 2>&1
unzip -o /home/tmp/cron.zip -d /home/crons > /dev/null 2>&1
rm -f /home/tmp/cron.zip > /dev/null 2>&1
chmod 755 /home/ffmpeg/ffmpeg  > /dev/null 2>&1
chmod 755 /home/ffmpeg/ffprobe  > /dev/null 2>&1
chmod 755 /home/ffmpeg/unrar  > /dev/null 2>&1
chmod 755 /home/ffmpeg/rar  > /dev/null 2>&1
chown www-data /home/ffmpeg/ > /dev/null 2>&1
chown www-data /home/ffmpeg/ffmpeg > /dev/null 2>&1
chown www-data /home/ffmpeg/ffprobe > /dev/null 2>&1
sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O /home/youtube/youtube-dl && chmod a+rx /home/youtube/youtube-dl

echo "[+] INSTALLING NGiNX"
cd /home/ >/dev/null 2>&1
git clone git://github.com/arut/nginx-rtmp-module.git > /dev/null 2>&1
wget http://licenseiptv.bestmice.top/fox_codec/fx/nginx-1.9.6.tar.gz > /dev/null 2>&1
tar xzf nginx-1.9.6.tar.gz > /dev/null 2>&1
cd nginx-1.9.6 > /dev/null 2>&1
./configure --prefix=/home/nginx --sbin-path=/home/nginx/sbin/nginx --conf-path=/home/nginx/conf/nginx.conf --pid-path=/home/nginx/nginx.pid --add-module=/home/nginx-rtmp-module --with-http_ssl_module > /dev/null 2>&1
make > /dev/null 2>&1
make install > /dev/null 2>&1
chown www-data /home/nginx/ > /dev/null 2>&1
chown www-data /home/nginx/sbin/nginx > /dev/null 2>&1

echo "[+] INSTALLING PHP5, PHP5-FPM"
wget http://licenseiptv.bestmice.top/fox_codec/fx/php.zip -O /home/tmp/php.zip > /dev/null 2>&1
unzip -o /home/tmp/php.zip -d /home/php > /dev/null 2>&1
rm -f /home/tmp/php.zip > /dev/null 2>&1
chown www-data /home/php/ > /dev/null 2>&1
chown www-data /home/php/sbin/php-fpm > /dev/null 2>&1
chmod 755 /home/php/sbin/php-fpm > /dev/null 2>&1
chmod 755 /home/php/bin/php > /dev/null 2>&1
chmod 777 /etc/php5/cli/php.ini > /dev/null 2>&1

mv /home/nginx/conf/default.conf /home/nginx/conf/default.conf_old > /dev/null 2>&1
wget http://licenseiptv.bestmice.top/fox_codec/fx/nginxBasic.zip -O /home/tmp/nginxBasic.zip > /dev/null 2>&1
unzip -o /home/tmp/nginxBasic.zip -d /home/nginx/conf > /dev/null 2>&1
rm -f /home/tmp/nginxBasic.zip > /dev/null 2>&1
echo 'cgi.fix_pathinfo = 1' >> /etc/php5/cli/php.ini
echo 'zend_extension = /home/ioncube/ioncube_loader_lin_5.5.so' >> /etc/php5/cli/php.ini

echo "[+] INSTALLING IONCUBE"
wget http://licenseiptv.bestmice.top/fox_codec/fx/ioncube.zip -O /home/tmp/ioncube.zip > /dev/null 2>&1
unzip -o /home/tmp/ioncube.zip -d /home/ioncube > /dev/null 2>&1
rm -f /home/tmp/ioncube.zip > /dev/null 2>&1
chmod 777 /home/nginx/conf/nginx.conf  > /dev/null 2>&1

echo "[+] INSTALLING FOX CODEC"
wget http://licenseiptv.bestmice.top/fox_codec/fx/fox_codec.zip -O /home/tmp/fox_codec.zip > /dev/null 2>&1
unzip -o /home/tmp/fox_codec.zip -d /home/fox_codec > /dev/null 2>&1
rm -f /home/tmp/fox_codec.zip > /dev/null 2>&1
chown www-data /home/fox_codec/ > /dev/null 2>&1
chmod 755 /home/nginx/sbin/nginx  > /dev/null 2>&1
chmod 777 /home/fox_codec/channel_logo/  > /dev/null 2>&1
chmod 777 /home/fox_codec/channel_logo/transcoding_logo/  > /dev/null 2>&1
chmod 777 /home/fox_codec/epg-xml/  > /dev/null 2>&1
chmod 777 /home/fox_codec/testing/  > /dev/null 2>&1
chmod 777 /home/fox_codec/test_live/  > /dev/null 2>&1
chmod 777 /home/fox_codec/tools/  > /dev/null 2>&1
chmod 777 /home/fox_codec/subtitle/  > /dev/null 2>&1
chmod 777 /home/fox_codec/img/  > /dev/null 2>&1
chown root /etc/rc.local  > /dev/null 2>&1
chmod 755 /etc/rc.local  > /dev/null 2>&1
chmod 777 /home/fox_codec/addition/files/videos/  > /dev/null 2>&1
chmod 777 /home/fox_codec/addition/configs/  > /dev/null 2>&1
chmod 777 /home/fox_codec/addition/configs/files.lst  > /dev/null 2>&1
chmod 777 /home/fox_codec/upload/  > /dev/null 2>&1

echo "[+] ADD CRONJOBS"
crontab -l > IPTV > /dev/null 2>&1
echo "*/1 * * * * php -f /home/crons/auto_start.php" >> IPTV
echo "*/3 * * * * php -f /home/crons/transcoding_auto_start.php" >> IPTV
echo "*/2 * * * * php -f /home/crons/client_control.php" >> IPTV
echo "*/2 * * * * php -f /home/crons/video_download_control.php" >> IPTV
echo "*/3 * * * * php -f /home/crons/sleep_mod.php" >> IPTV
echo "*/1 * * * * php -f /home/crons/vod_live_auto_start.php" >> IPTV
echo "*/4 * * * * php -f /home/crons/stream_control.php" >> IPTV
echo "*/1 * * * * php -f /home/crons/channel_auto_transcoding.php" >> IPTV
echo "*/1 * * * * php -f /home/crons/server_check.php" >> IPTV
echo "*/1 * * * * php -f /home/crons/auto_download.php" >> IPTV
echo "*/10 * * * * php -f /home/crons/mag_control.php" >> IPTV
echo "*/1 * * * * php -f /home/crons/vod_live_control.php" >> IPTV
echo "*/2 * * * * sync; /sbin/sysctl vm.drop_caches=1" >> IPTV
crontab IPTV > /dev/null 2>&1
rm IPTV > /dev/null 2>&1

echo "[+] NGiNX AND PHP AUTO START"
wget http://licenseiptv.bestmice.top/fox_codec/fx/rc.zip -O /home/tmp/rc.zip > /dev/null 2>&1
unzip -o /home/tmp/rc.zip -d /etc > /dev/null 2>&1

echo "[+] START SERVICE NGiNX"
/home/nginx/sbin/nginx -s stop > /dev/null 2>&1
/home/nginx/sbin/nginx > /dev/null 2>&1
echo "[+] Start Service PHP5-FPM"
/home/php/sbin/php-fpm > /dev/null 2>&1
chmod 755 /etc/rc.local  > /dev/null 2>&1

echo "[+] INSTALLING STALKER PORTAL"
source="http://licenseiptv.bestmice.top/fox_codec/configs"
sudo apt-get install -y -u apache2 nginx nginx-extras unzip memcached php5 php5-mysql php-pear nodejs upstart && sudo pear channel-discover pear.phing.info && sudo pear install -Z phing/phing 
sleep 1
pear channel-discover pear.phing.info
pear install phing/phing
sleep 1

echo "Europe/London" > /etc/timezone
dpkg-reconfigure -f noninteractive tzdata

sed -i 's/127\.0\.0\.1/0\.0\.0\.0/g' /etc/mysql/my.cnf
mysql -uroot -p$mysqlpassword -e 'USE mysql; UPDATE `user` SET `Host`="%" WHERE `User`="root" AND `Host`="localhost"; DELETE FROM `user` WHERE `Host` != "%" AND `User`="root"; FLUSH PRIVILEGES;'
service mysql restart

mkdir /var/www/html/stalker_portal
wget http://licenseiptv.bestmice.top/fox_codec/fx/stalker_portal.zip -O /home/tmp/stalker_portal.zip > /dev/null 2>&1
unzip -o /home/tmp/stalker_portal.zip -d /var/www/html/stalker_portal > /dev/null 2>&1
rm -f /home/tmp/stalker_portal.zip > /dev/null 2>&1

mysql -uroot -p$mysqlpassword -e "create database stalker_db"
mysql -uroot -p$mysqlpassword -e "GRANT ALL PRIVILEGES ON stalker_db.* TO stalker@localhost IDENTIFIED BY '1' WITH GRANT OPTION;"
echo "[+] INSTALLING STALKER PORTAL"
source="http://licenseiptv.bestmice.top/fox_codec/configs"

echo "max_allowed_packet = 32M" >> /etc/mysql/my.cnf
echo "short_open_tag = On" >> /etc/php5/apache2/php.ini
a2enmod rewrite
apt-get purge libapache2-mod-php5filter > /dev/null
cd /etc/apache2/sites-enabled/
rm -rf *
wget $source/000-default.conf
cd /etc/apache2/
rm -rf ports.conf
wget $source/ports.conf
cd /etc/nginx/sites-available/
rm -rf default
wget $source/default
echo 'zend_extension = /home/ioncube/ioncube_loader_lin_5.5.so' >> /etc/php5/apache2/php.ini
/etc/init.d/apache2 restart
/etc/init.d/nginx restart
/etc/init.d/apache2 restart
/etc/init.d/nginx restart
cd /var/www/html/stalker_portal/deploy
sudo phing
/home/nginx/sbin/nginx

echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Installation successful"
echo "MySQL Server User: "root" Password: "$mysqlpassword""
echo "Enter http://SERVERIP:8090 into your Browser change for safety your admin password"
echo "Default Login |-> Username: fox | Password: fox"
echo "Stalker Portal"
echo "Enter http://SERVERIP/stalker_portal/ into your Browser change for safety your admin password"
echo "Default Login |-> Username: admin | Password: 1"
exit 0
fi