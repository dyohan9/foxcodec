
<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

class DiskStatus
{
	const RAW_OUTPUT = true;

	private $diskPath;

	public function __construct($diskPath)
	{
		$this->diskPath = $diskPath;
	}

	public function totalSpace($rawOutput = false)
	{
		$diskTotalSpace = @disk_total_space($this->diskPath);

		if ($diskTotalSpace === false) {
			throw new Exception('totalSpace(): Invalid disk path.');
		}
		if ($rawOutput) {
		}
		else {
		}

		return $this->addUnits($diskTotalSpace);
	}

	public function freeSpace($rawOutput = false)
	{
		$diskFreeSpace = @disk_free_space($this->diskPath);

		if ($diskFreeSpace === false) {
			throw new Exception('freeSpace(): Invalid disk path.');
		}
		if ($rawOutput) {
		}
		else {
		}

		return $this->addUnits($diskFreeSpace);
	}

	public function usedSpace($precision = 1)
	{
		try {
			return round(100 - (($this->freeSpace(self::RAW_OUTPUT) / $this->totalSpace(self::RAW_OUTPUT)) * 100), $precision);
		}
		catch (Exception $e) {
			throw $e;
		}
	}

	public function getDiskPath()
	{
		return $this->diskPath;
	}

	private function addUnits($bytes)
	{
		$units = array('B', 'KB', 'MB', 'GB', 'TB');
		$i = 0;

		$bytes /= 1024;
		++$i;

		return round($bytes, 1) . ' ' . $units[$i];
	}
}

echo '      ';

?>
