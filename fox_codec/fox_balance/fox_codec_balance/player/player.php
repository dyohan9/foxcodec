
<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

include '/home/fox_codec/config.php';
$stream = $_GET['stream'];
$username = htmlentities($_GET['username'], ENT_QUOTES, 'UTF-8');
$password = htmlentities($_GET['password'], ENT_QUOTES, 'UTF-8');
if (!isset($_GET['username']) || !isset($_GET['password']) || !isset($_GET['stream'])) {
	exit('Error');
}

if (!is_numeric($_GET['stream'])) {
	exit('Error');
}

$server_dns_name = MAIN_IP;
$server_port = MAIN_PORT;
$live_url = 'http://' . $server_dns_name . ':' . $server_port . '/hls/' . $username . '/' . $password . '/' . $stream . '.m3u8';
echo '<!doctype html>' . "\n" . '<head>' . "\n" . '   <!-- player skin -->' . "\n" . '   <link rel="stylesheet" href="/player/skin/skin.css">' . "\n\n" . '   <!-- site specific styling -->' . "\n" . '   <!-- for video tag based installs flowplayer depends on jQuery 1.7.2+ -->' . "\n" . '   <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>' . "\n\n" . '   <!-- include flowplayer -->' . "\n" . '   <script src="/player/flowplayer.min.js"></script>' . "\n" . '<script src="https://releases.flowplayer.org/hlsjs/flowplayer.hlsjs.light.min.js"></script>' . "\n" . '<script type="text/javascript">' . "\n" . 'flowplayer.conf.embed = false;' . "\n" . 'flowplayer.conf.fullscreen = true;' . "\n" . '</script>' . "\n" . '<div class="preloader"></div>' . "\n" . '</head>' . "\n" . '<body>' . "\n" . '   <!-- the player -->' . "\n" . '   <div data-live="true" class="flowplayer" data-swf="/player/flowplayerhls.swf" data-ratio="0.525">' . "\n" . '     <video autoplay data-title="Live stream">' . "\n\t\t" . '<source type="application/x-mpegurl"' . "\n" . '        src="';
echo $live_url;
echo '">' . "\n" . '   </video>' . "\n" . '   </div>' . "\n\n\t" . '</body>' . "\n" . '</html>' . "\n";

?>
