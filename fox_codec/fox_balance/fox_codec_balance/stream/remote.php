
<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

set_time_limit(0);
header('Access-Control-Allow-Origin: *');
header('X-Accel-Buffering: no');
$url = $_GET['url'];
$content = $url;
header('Content-type: application/octet-stream');
header('Location: ' . $content);

?>
