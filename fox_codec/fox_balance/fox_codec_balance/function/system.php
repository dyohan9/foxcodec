
<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

function get_boottime()
{
	if (file_exists('/proc/uptime') && is_readable('/proc/uptime')) {
		$tmp = explode(' ', file_get_contents('/proc/uptime'));
		return secondsToTime(intval($tmp[0]));
	}

	return '';
}

function secondsToTime($inputSeconds)
{
	$secondsInAMinute = 60;
	$secondsInAnHour = 60 * $secondsInAMinute;
	$secondsInADay = 24 * $secondsInAnHour;
	$days = (int) floor($inputSeconds / $secondsInADay);
	$hourSeconds = $inputSeconds % $secondsInADay;
	$hours = (int) floor($hourSeconds / $secondsInAnHour);
	$minuteSeconds = $hourSeconds % $secondsInAnHour;
	$minutes = (int) floor($minuteSeconds / $secondsInAMinute);
	$remainingSeconds = $minuteSeconds % $secondsInAMinute;
	$seconds = (int) ceil($remainingSeconds);
	$final = '';

	if ($days != 0) {
		$final .=  . $days . 'd ';
	}

	if ($hours != 0) {
		$final .=  . $hours . 'h ';
	}

	if ($minutes != 0) {
		$final .=  . $minutes . 'm ';
	}

	$final .=  . $seconds . 's';
	return $final;
}

function get_server_ram_usage()
{
	$exec_free = explode("\n", trim(shell_exec('free')));
	$get_mem = preg_split('/[\\s]+/', $exec_free[1]);
	$mem = round(($get_mem[2] / $get_mem[1]) * 100, 0) . '%';
	return $mem;
}

function get_server_cpu_usage()
{
	$load = sys_getloadavg();
	return $load[0];
}

function GetTotalCPUsage()
{
	$total_cpu = intval(shell_exec('ps aux|awk \'NR > 0 { s +=$3 }; END {print s}\''));
	$cores = intval(shell_exec('grep --count processor /proc/cpuinfo'));
	return intval($total_cpu / $cores);
}

include '/home/fox_codec/config.php';
$islem = $_GET['islem'];
$network = $_GET['network'];

if ($islem == 'stats') {
	$json = array();
	$json['aktif'] = shell_exec('sudo ps ax | grep -v grep | grep -c /home/ffmpeg/ffmpeg');
	$json['cpu'] = get_server_cpu_usage();
	$json['ram'] = get_server_ram_usage();
	$json['bytes_sent'] = 0;
	$json['bytes_received'] = 0;
	$json['uptime'] = get_boottime();

	if (file_exists('/sys/class/net/' . $network . '/statistics/tx_bytes')) {
		$bytes_sent_old = trim(file_get_contents('/sys/class/net/' . $network . '/statistics/tx_bytes'));
		$bytes_received_old = trim(file_get_contents('/sys/class/net/' . $network . '/statistics/rx_bytes'));
		sleep(1);
		$bytes_sent_new = trim(file_get_contents('/sys/class/net/' . $network . '/statistics/tx_bytes'));
		$bytes_received_new = trim(file_get_contents('/sys/class/net/' . $network . '/statistics/rx_bytes'));
		$total_bytes_sent = round((($bytes_sent_new - $bytes_sent_old) / 1024) * 0.0078125, 2);
		$total_bytes_received = round((($bytes_received_new - $bytes_received_old) / 1024) * 0.0078125, 2);
		$date_end = 0;
		$sorgu = mysql_query('select count(id) as sayi from activity where date_end="' . $date_end . '"');
		$gelen = mysql_fetch_assoc($sorgu);

		if (0 < $gelen['sayi']) {
			$json['bytes_sent'] = $total_bytes_sent;
		}
		else {
			$json['bytes_sent'] = 0;
		}

		if ($json['aktif'] == 0) {
			$json['bytes_received'] = 0;
		}
		else {
			$json['bytes_received'] = $total_bytes_received;
		}
	}

	echo json_encode($json);
}

if ($islem == 'channels_stats') {
	$json = array();
	$json['aktif'] = shell_exec('sudo ps ax | grep -v grep | grep -c /home/ffmpeg/ffmpeg');
	$vods = mysql_query('select count(*) from movie_vod');
	$vods_sayi = mysql_fetch_array($vods);
	$aktif_izle = mysql_query('select count(*) from activity Where date_end=0');
	$aktif_izley = mysql_fetch_array($aktif_izle);
	$json['aktif_izleyici'] = $aktif_izley[0];
	$json['vods_sayi'] = $vods_sayi[0];
	echo json_encode($json);
}

mysql_close($baglan);

?>
