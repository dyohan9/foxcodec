
<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

function ps_running($pid)
{
	if (empty($pid)) {
		return false;
	}

	return file_exists('/proc/' . $pid);
}

function seo($s)
{
	$tr = array('ş', 'Ş', 'ı', 'I', 'İ', 'ğ', 'Ğ', 'ü', 'Ü', 'ö', 'Ö', 'Ç', 'ç', '(', ')', '/', ':', ',');
	$eng = array('s', 's', 'i', 'i', 'i', 'g', 'g', 'u', 'u', 'o', 'o', 'c', 'c', '', '', '-', '-', '');
	$s = str_replace($tr, $eng, $s);
	$s = strtolower($s);
	$s = preg_replace('/&amp;amp;amp;amp;amp;amp;amp;amp;amp;.+?;/', '', $s);
	$s = preg_replace('/\\s+/', '-', $s);
	$s = preg_replace('|-+|', '-', $s);
	$s = preg_replace('/#/', '', $s);
	$s = str_replace('.', '', $s);
	$s = trim($s, '-');
	return $s;
}

function klasor_sil($_KLASOR)
{
	if (substr($_KLASOR, strlen($_KLASOR) - 1, 1) != '/') {
		$_KLASOR .= '/';
	}

	if ($handle = opendir($_KLASOR)) {
		while ($_OBJ = readdir($handle)) {
			if (($_OBJ != '.') && ($_OBJ != '..')) {
				if (is_dir($_KLASOR . $_OBJ)) {
					if (!KlasorSil($_KLASOR . $_OBJ)) {
						return false;

						if (is_file($_KLASOR . $_OBJ)) {
							if (!unlink($_KLASOR . $_OBJ)) {
								return false;
							}
						}
					}
				}
				else {
					return false;
					return false;
				}
			}
		}

		CLOSEDIR($handle);

		if (!@rmdir($_KLASOR)) {
			return false;
		}

		return true;
	}

	return false;
}


?>
