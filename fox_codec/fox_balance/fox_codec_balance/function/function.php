
<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.4
 * @ Release on : 24.03.2018
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 **/

function kullanici($uzunluk)
{
	$karakterler = '1234567890abcdefghijklmnopqrstuvwxyz';
	$i = 0;

	while ($i < $uzunluk) {
		$key .= $karakterler[rand(0, 35)];
		++$i;
	}

	return $key;
}

function parola($uzunluk)
{
	$karakterler = '1234567890abcdefghijklmnopqrstuvwxyz';
	$i = 0;

	while ($i < $uzunluk) {
		$key .= $karakterler[rand(0, 35)];
		++$i;
	}

	return $key;
}

function getUserCountry($ip, $key)
{
	$url = 'http://api.wipmania.com/' . $ip . '?k=' . $key . '&t=json';
	$ch = curl_init();
	$headers = 'Typ: phpcurl' . "\r\n";
	$headers .= 'Ver: 1.0' . "\r\n";
	$headers .= 'Connection: Close' . "\r\n\r\n";
	$timeout = 5;
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array($headers));
	$content = curl_exec($ch);
	curl_close($ch);
	return $content;
}

function getRealIpAddr()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	}
	else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}

	return $ip;
}

function cleanInput($input)
{
	$search = array('@<script[^>]*?>.*?</script>@si', '@<[\\/\\!]*?[^<>]*?>@si', '@<style[^>]*?>.*?</style>@siU', '@<![\\s\\S]*?--[ \\t\\n\\r]*>@');
	$output = preg_replace($search, '', $input);
	return $output;
}

function sanitize($input)
{
	if (is_array($input)) {
		foreach ($input as $var => $val) {
			$output[$var] = sanitize($val);
		}
	}
	else {
		if (get_magic_quotes_gpc()) {
			$input = stripslashes($input);
		}

		$input = cleaninput($input);
		$output = mysql_real_escape_string($input);
	}

	return $output;
}

function kisalt($kelime, $str = 10)
{
	if ($str < strlen($kelime)) {
		if (function_exists('mb_substr')) {
			$kelime = mb_substr($kelime, 0, $str, 'UTF-8') . '..';
		}
		else {
			$kelime = substr($kelime, 0, $str) . '..';
		}
	}

	return $kelime;
}

function ps_running($pid)
{
	if (empty($pid)) {
		return false;
	}

	return file_exists('/proc/' . $pid);
}

function url_var($adres)
{
	$ch = curl_init($adres);
	curl_setopt($ch, CURLOPT_NOBODY, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_exec($ch);
	$retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);

	if ($retcode == 200) {
		return true;
	}

	return false;
}

function uzanti($x)
{
	$i = pathinfo($x);
	return $i['extension'];
}

function seflink($text)
{
	$find = array('Ç', 'Ş', 'Ğ', 'Ü', 'İ', 'Ö', 'ç', 'ş', 'ğ', 'ü', 'ö', 'ı', '+', '#');
	$replace = array('c', 's', 'g', 'u', 'i', 'o', 'c', 's', 'g', 'u', 'o', 'i', 'plus', 'sharp');
	$text = strtolower(str_replace($find, $replace, $text));
	$text = preg_replace('@[^A-Za-z0-9\\-_\\.\\+]@i', ' ', $text);
	$text = trim(preg_replace('/\\s+/', ' ', $text));
	$text = str_replace(' ', '-', $text);
	return $text;
}

function klasor_sil($_KLASOR)
{
	if (substr($_KLASOR, strlen($_KLASOR) - 1, 1) != '/') {
		$_KLASOR .= '/';
	}

	if ($handle = opendir($_KLASOR)) {
		while ($_OBJ = readdir($handle)) {
			if (($_OBJ != '.') && ($_OBJ != '..')) {
				if (is_dir($_KLASOR . $_OBJ)) {
					if (!KlasorSil($_KLASOR . $_OBJ)) {
						return false;

						if (is_file($_KLASOR . $_OBJ)) {
							if (!unlink($_KLASOR . $_OBJ)) {
								return false;
							}
						}
					}
				}
				else {
					return false;
					return false;
				}
			}
		}

		CLOSEDIR($handle);

		if (!@rmdir($_KLASOR)) {
			return false;
		}

		return true;
	}

	return false;
}

function seo($s)
{
	$tr = array('ş', 'Ş', 'ı', 'I', 'İ', 'ğ', 'Ğ', 'ü', 'Ü', 'ö', 'Ö', 'Ç', 'ç', '(', ')', '/', ':', ',');
	$eng = array('s', 's', 'i', 'i', 'i', 'g', 'g', 'u', 'u', 'o', 'o', 'c', 'c', '', '', '-', '-', '');
	$s = str_replace($tr, $eng, $s);
	$s = strtolower($s);
	$s = preg_replace('/&amp;amp;amp;amp;amp;amp;amp;amp;amp;.+?;/', '', $s);
	$s = preg_replace('/\\s+/', '-', $s);
	$s = preg_replace('|-+|', '-', $s);
	$s = preg_replace('/#/', '', $s);
	$s = str_replace('.', '', $s);
	$s = trim($s, '-');
	return $s;
}

function folderSize($dir)
{
	$size = 0;

	foreach (glob(rtrim($dir, '/') . '/*', GLOB_NOSORT) as $each) {
		if (is_file($each)) {
		}
		else {
		}

		$size += folderSize($each);
	}

	return $size;
}

function dosya_boyut($dosya_boyutum)
{
	$si_prefix = array('B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB');
	$base = 1024;
	$class = min((int) log($dosya_boyutum, $base), count($si_prefix) - 1);
	$boyut = sprintf('%1.2f', $dosya_boyutum / pow($base, $class)) . ' ' . $si_prefix[$class] . '';
	return $boyut;
}

function GetStreamBitrate($type, $path, $force_duration = NULL)
{
	$birrate = 0;

	if (!file_exists($path)) {
		return $bitrate;
	}

	switch ($type) {
	case 'movie':
		if (!is_null($force_duration)) {
			sscanf($force_duration, '%d:%d:%d', $hours, $minutes, $seconds);
			$time_seconds = (isset($seconds) ? ($hours * 3600) + ($minutes * 60) + $seconds : ($hours * 60) + $minutes);
			$bitrate = round((filesize($path) * 0.0080000000000000002) / $time_seconds);
		}

		break;
	}

	switch ($type) {
	case 'live':
		$fp = fopen($path, 'r');
		$bitrates = array();

		if (!feof($fp)) {
			$line = trim(fgets($fp));
			list($trash, $seconds) = explode(':', $line);
			$seconds = rtrim($seconds, ',');
			$segment_file = trim(fgets($fp));

			if (!file_exists(dirname($path) . '/' . $segment_file)) {
				break;
			}

			$segment_size_in_kilobits = filesize(dirname($path) . '/' . $segment_file) * 0.0080000000000000002;
			$bitrates[] = $segment_size_in_kilobits / $seconds;
			break;
		}

		fclose($fp);

		if (0 < count($bitrates)) {
		}
		else {
		}

		$bitrate = 0;
	}

	return $bitrate;
}

function isDomainAvailible($domain)
{
	if (!filter_var($domain, FILTER_VALIDATE_URL)) {
		return false;
	}

	$curlInit = curl_init($domain);
	curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, 5);
	curl_setopt($curlInit, CURLOPT_TIMEOUT, 5);
	curl_setopt($curlInit, CURLOPT_HEADER, true);
	curl_setopt($curlInit, CURLOPT_NOBODY, true);
	curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($curlInit);
	curl_close($curlInit);

	if ($response) {
		return true;
	}

	return false;
}

function codec_bul($server_ip, $server_port, $stream_id, $restream, $channel_path_1)
{
	$json = array();
	$json['actions'] = 'channel_codec__bul';
	$json['stream_id'] = $stream_id;
	$json['restream'] = $restream;
	$json['channel_path_1'] = $channel_path_1;
	$data_string = json_encode($json);
	$ch = curl_init('http://' . $server_ip . ':' . $server_port . '/api/data_api.php');
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data_string)));
	$result = curl_exec($ch);
}

function pid_control($pid)
{
	exec('sudo ps -C ffmpeg -o pid', $a);

	if (in_array($pid, $a)) {
		$status = 1;
	}
	else {
		$status = 0;
	}

	return $status;
}

function redirect($file_names)
{
	$file_name = $file_names;
	header('Content-Type: application/octet-stream');
	header('Location: ' . $file_name);
	exit();
}

function pids_controls($pid, $user_activity_id)
{
	exec('sudo ps -C php-fpm -o pid', $a);

	if (in_array($pid, $a)) {
	}
	else {
		mysql_query('delete from activity where id=\'' . $user_activity_id . '\'');
	}
}

error_reporting(0);
session_start();
ob_start();

?>
