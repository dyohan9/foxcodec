/*
Navicat MySQL Data Transfer

Source Server         : bizim
Source Server Version : 50559
Source Host           : 144.217.165.19:3306
Source Database       : iptv

Target Server Type    : MYSQL
Target Server Version : 50559
File Encoding         : 65001

Date: 2018-02-05 11:50:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for activity
-- ----------------------------
DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `stream_id` int(11) NOT NULL,
  `str_type` int(11) DEFAULT '1',
  `user_ip` varchar(255) NOT NULL,
  `country` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `pid` int(11) NOT NULL,
  `date_start` varchar(255) NOT NULL,
  `date_end` varchar(500) DEFAULT NULL,
  `container` varchar(255) DEFAULT NULL,
  `last_ts` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `date_end` (`date_end`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of activity
-- ----------------------------

-- ----------------------------
-- Table structure for channels
-- ----------------------------
DROP TABLE IF EXISTS `channels`;
CREATE TABLE `channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `channel_logo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `channel_path_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `channel_path_2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `channel_path_3` varchar(255) DEFAULT NULL,
  `server_id` int(11) DEFAULT '1',
  `category_id` int(11) DEFAULT NULL,
  `transcode_profiles` int(11) DEFAULT '0',
  `transcoding` int(11) DEFAULT '0',
  `sort` int(255) DEFAULT '0',
  `count` int(11) DEFAULT '0',
  `pid` int(11) DEFAULT '0',
  `stream_status` int(11) DEFAULT '0',
  `start_time` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `stop_time` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `pid_control` int(11) DEFAULT '0',
  `pid_control_ok` int(11) DEFAULT '0',
  `control_count` int(11) DEFAULT '0',
  `output` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `sleep_mod` int(255) DEFAULT '0',
  `channel_type` int(11) DEFAULT '1',
  `channel_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `working_channel` int(11) DEFAULT '1',
  `ch_id` int(11) DEFAULT NULL,
  `restream` int(11) NOT NULL DEFAULT '0',
  `restream_status` int(11) DEFAULT '0',
  `sira` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`restream`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of channels
-- ----------------------------

-- ----------------------------
-- Table structure for movie_vod
-- ----------------------------
DROP TABLE IF EXISTS `movie_vod`;
CREATE TABLE `movie_vod` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `server_id` int(11) DEFAULT '1',
  `movie_path` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `movie_path_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `movie_type` int(11) DEFAULT NULL,
  `movie_format` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `movie_size` varchar(255) CHARACTER SET utf8 DEFAULT '0',
  `movie_pid` int(11) DEFAULT '0',
  `download_status` int(11) DEFAULT '0',
  `download_ok` int(11) DEFAULT '0',
  `sort` int(255) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `movie_video_codec` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `movie_auido_codec` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of movie_vod
-- ----------------------------
