<?php
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
require("modules/Core.php");

$balance = array("balance" => null);

if(isset($_GET)){
    if(isset($_GET["ip"])){

        $Core = new Core();

        $result = $Core->queryPDO("select load_balance from licenses where serverIP = :serverip;",
            array(
                ":serverip" => $_GET["ip"]
            )
        )->fetch(PDO::FETCH_ASSOC);

        if($result){
            $balance["balance"] = $result["load_balance"];
        }

    }
}

echo json_encode($balance);

?>