<?php
require("modules/Core.php");
$Core = new Core();

if(isset($_POST)){

    if(isset($_POST["serverIP"]) && isset($_POST["time"])){

        $check = $Core->queryPDO(/** @lang text */
            "SELECT * FROM licenses WHERE serverIP = :ip",
            array(
                    ":ip" => $_POST["serverIP"])
        )->fetch(PDO::FETCH_ASSOC);

        if(!$check){
            $result = $Core->queryPDO(/** @lang text */
                "INSERT INTO licenses (serverIP, licenseKey, activationKey, load_balance, validate_date) VALUES (:serverip, :licensekey, :activationKey, :load_balance, :validate_date);",
                array(
                    ":serverip" => $_POST["serverIP"],
                    ":licensekey" => $Core->generateKey($_POST["serverIP"]),
                    ":activationKey" => $Core->generateActivation($_POST["serverIP"]),
                    ":load_balance" => $_POST["load_balance"],
                    ":validate_date" => date('Y-m-d H:i:s', strtotime("+".$_POST["time"]." days"))
                )
            );

            if($result) {
                echo '<script>alert("Licença adicionada com sucesso.");</script>';
            }else{
                echo '<script>alert("Licença não foi adicionada por algum erro interno.");</script>';
            }

        }else{
            echo '<script>alert("Já existe uma licença cadastrada nesse ip.");</script>';
        }

    }

    if(isset($_POST["deleteLicense"])){

        $check = $Core->queryPDO(/** @lang text */
            "SELECT * FROM licenses WHERE serverIP = :ip",
            array(
                ":ip" => $_POST["deleteLicense"])
        )->fetch(PDO::FETCH_ASSOC);

        if($check) {
            $result = $Core->queryPDO(/** @lang text */
                "delete from licenses where serverIP = :ip;",
                array(
                    ":ip" => $_POST["deleteLicense"],
                )
            );

            if ($result) {
                echo '<script>alert("Licença removida com sucesso.");</script>';
            } else {
                echo '<script>alert("Licença não foi removida por algum erro interno.");</script>';
            }

        }else{
            echo '<script>alert("Licença não encontrada.");</script>';
        }

    }

}

?>

<!DOCTYPE html>
<html lang='en' class=''>
<head>
    <meta charset='UTF-8'>
    <meta name="robots" content="noindex">
    <link rel="shortcut icon" type="image/x-icon" href="//production-assets.codepen.io/assets/favicon/favicon-8ea04875e70c4b0bb41da869e81236e54394d63638a1ef12fa558a4a835f1164.ico" />
    <link rel="mask-icon" type="" href="//production-assets.codepen.io/assets/favicon/logo-pin-f2d2b6d2c61838f7e76325261b7195c27224080bc099486ddd6dccb469b8e8e6.svg" color="#111" />
    <link rel="canonical" href="https://codepen.io/ace-subido/pen/Cuiep" />
    <link rel='stylesheet prefetch' href='//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>
    <style class="cp-pen-styles">body {
            background: #eee !important;
        }
        .wrapper {
            margin-top: 80px;
            margin-bottom: 80px;
        }
        .form-signin {
            max-width: 800px;
            padding: 15px 35px 45px;
            margin: 0 auto;
            background-color: #fff;
            border: 1px solid rgba(0, 0, 0, 0.1);
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 30px;
        }
        .form-signin .form-control {
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        .form-signin .form-control:focus {
            z-index: 2;
        }
        .form-signin input[type="text"] {
            margin-bottom: -1px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
            margin-bottom: 20px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
    </style>
</head>
<body>
</br></br>
<center><h2 class="form-signin-heading">Painel</h2></center>

<div class="wrapper">

    <form class="form-signin" method="post">
        <h2 class="form-signin-heading">Adicionar Servidor</h2>
        <input type="text" class="form-control" name="serverIP" placeholder="IP do Servidor" required=""/>
        <input type="text" class="form-control" name="load_balance" placeholder="Total de servidores disponíveis" required=""/>
        <input type="text" class="form-control" name="time" placeholder="Tempo em quantidade de dias" required=""/>
        </br>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Cadastrar Servidor</button>

    </form>

    </br>

    <form class="form-signin" method="post">
        <h2 class="form-signin-heading">Remover Servidor</h2>
        <select class="form-control" name="deleteLicense" required>
            <?php
            foreach ($Core->listAllKeys() as $key => $value) {
                echo '<option value="' . $value["serverIP"] . '">' . $key . ' - '.$value["serverIP"].'</option>';
            }
            ?>

        </select></br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Remover Servidor</button>

    </form>

    </br>

    <form class="form-signin" method="post">
        <h2 class="form-signin-heading">Lista de Servidores</h2>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>IP</th>
                <th>License Key</th>
                <th>Activation Key</th>
                <th>Load Balance</th>
                <th>Validade</th>
            </tr>
            </thead>
            <tbody>

            <?php
            foreach ($Core->listAllKeys() as $key => $value) {
                echo /** @lang text */
                    '<tr >
                    <td > '.$value["serverIP"].'</td >
                    <td > '.$value["licenseKey"].'</td >
                    <td > '.$value["activationKey"].'</td >
                    <td > '.$value["load_balance"].'</td >
                    <td>'.date("d/m/Y", strtotime($value["validate_date"])).'</td>
                    </tr >';
            }
            ?>

            </tbody>
        </table>

    </form>

</div>
</body>
</html>