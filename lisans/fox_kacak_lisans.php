<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 18/03/2018
 * Time: 18:39
 */
require("../modules/Core.php");
header("Access-Control-Allow-Origin: *");

if(file_get_contents('php://input')){
    $result = json_decode(file_get_contents('php://input'), true);

    if(isset($result["domain"]) && isset($result["port"]) && isset($result["username"]) && isset($result["password"]) && isset($result["tarih"])){
        echo "foi";
        $Core = new Core();
        $Core->queryPDO("INSERT INTO history_fail (domain, port, username, password, date) VALUES (:domain, :port, :username, :password, :date);",
            array(
                ":domain" => $result["domain"],
                ":port" => $result["port"],
                ":username" => $result["username"],
                ":password" => $result["password"],
                "date" => date('Y-m-d H:i:s', strtotime($result["tarih"]))
            )
        );
    }
}

exit();
