<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 18/03/2018
 * Time: 18:39
 */
require("../modules/Core.php");
header("Access-Control-Allow-Origin: *");

if(isset($_GET)){
    if(isset($_GET["ip"]) && isset($_GET["port"]) && isset($_GET["lisans_key"])){

        $Core = new Core();

        $result = $Core->queryPDO("select * from licenses where serverIP = :serverip and licenseKey = :licensekey;",
            array(
                    ":serverip" => $_GET["ip"],
                    ":licensekey" => $_GET["lisans_key"]
            )
        )->fetch(PDO::FETCH_ASSOC);

        if($result){
            echo '<li>1</li>';
            exit();
        }
    }
}
echo '<li>0</li>';
exit();

?>
