<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 18/03/2018
 * Time: 21:17
 */

class Core
{

    private $host = "localhost";
    private $user = "root";
    private $pass = "99443543";
    private $dbname = "licence";
    private $connect;

    public function __construct()
    {

        $this->connect = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->pass);
        $this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        $this->connect->query("SET CHARACTER SET utf8");
        $this->connect->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND,'SET NAMES UTF8');
        date_default_timezone_set('America/Sao_Paulo');

    }

    public function queryPDO($sql, $params){
        $result = $this->connect->prepare($sql);

        foreach($params as $key => &$val){
            $result->bindParam($key, $val);
        }
        $result->execute();
        return $result;
    }

    public function isDomainAvailible($domain)
    {
        //check, if a valid url is provided
        if(!filter_var($domain, FILTER_VALIDATE_URL))
        {
            return false;
        }

        //initialize curl
        $curlInit = curl_init($domain);
        curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT,5);
        curl_setopt($curlInit, CURLOPT_TIMEOUT, 5);
        curl_setopt($curlInit,CURLOPT_HEADER,true);
        curl_setopt($curlInit,CURLOPT_NOBODY,true);
        curl_setopt($curlInit,CURLOPT_RETURNTRANSFER,true);

        //get answer
        $response = curl_exec($curlInit);

        curl_close($curlInit);

        if ($response) return true;

        return false;
    }

    public function generateKey($serverIP){
        $bass = "FOX";
        $sons = "-2018";
        $ms = "md5";
        $ss = "sha1";
        $hash = wordwrap(strtoupper($ss ($ss ($ss ($ss ($ms ($ss ($ss ($ms ($ms ($ss ($ss ($ms ($serverIP))))))))))))),5,'-',true);//lisans kodunu olustur ve 5 karakterde bir - koy
        $liskods = $hash;
        $cevirs=strrev($liskods);//lisans kodunu tersine cevir
        $lisansim = "$bass$cevirs$sons";

        return $lisansim;
    }

    public function generateActivation($serverIP){
        $acbass = "FOX";
        $acsons = "-2018";
        $mss = "md5";
        $sss = "sha1";
        $hash=wordwrap(strtoupper($mss ($mss ($sss ($sss ($mss ($sss ($sss ($mss ($mss ($sss ($sss ($mss ($sss ($serverIP)))))))))))))),5,'-',true);//lisans kodunu olustur ve 5 karakterde bir - koy
        $acliskods = $hash;
        $accevirs=strrev($acliskods);//lisans kodunu tersine cevir
        $aclisansim = "$acbass$accevirs$acsons";
        return $aclisansim;
    }

    public function listAllKeys(){
        return $this->queryPDO("select * from licenses;", array())->fetchAll(PDO::FETCH_ASSOC);
    }

}